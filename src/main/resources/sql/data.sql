insert into users(id, username, password, roles) values
(1, 'admin', '$2a$10$BoRZV6FzCOWEwKIwHsnajezG8sHVq3w.txmIZRfHhPh9yI5fV3sdK', 'ADMIN,USER' );

insert into buildings(id, username, password, roles) values
(1, 'admin', '$2a$10$BoRZV6FzCOWEwKIwHsnajezG8sHVq3w.txmIZRfHhPh9yI5fV3sdK', 'ADMIN,USER' );
