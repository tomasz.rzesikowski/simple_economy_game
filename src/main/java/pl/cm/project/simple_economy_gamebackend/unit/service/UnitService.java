package pl.cm.project.simple_economy_gamebackend.unit.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.cm.project.simple_economy_gamebackend.resource.model.Resource;
import pl.cm.project.simple_economy_gamebackend.resource.service.ResourceService;
import pl.cm.project.simple_economy_gamebackend.unit.model.Unit;
import pl.cm.project.simple_economy_gamebackend.unit.properties.UnitProperties;
import pl.cm.project.simple_economy_gamebackend.unit.repository.UnitRepository;

@Service
public class UnitService {
    private UnitRepository unitRepository;

    private ResourceService resourceService;

    private UnitProperties unitProperties;

    @Autowired
    public void setUnitRepository(UnitRepository unitRepository) {
        this.unitRepository = unitRepository;
    }

    @Autowired
    public void setResourceService(ResourceService resourceService) {this.resourceService = resourceService; }

    @Autowired
    public void setUnitProperties(UnitProperties unitProperties) {
        this.unitProperties = unitProperties;
    }

    public Unit findUnitByUserId(Integer userId){
        return unitRepository.findUnitByUser_Id(userId);
    }

    public boolean existsByUserId(Integer userId){
        return unitRepository.existsUnitByUser_Id(userId);
    }

    public boolean canRecruitUnit(Integer userId, String unitType, Long modValue) {
        Resource resource = resourceService.findResourceByUserId(userId);
        boolean canBuild = false;
        if (unitType.equals("gob") && resource.getMudQuantity() >= (unitProperties.getGoblinArcherMudCost() * modValue)) {
            canBuild = true;
        } else if (unitType.equals("orc") && resource.getStoneQuantity() >= (unitProperties.getOrcWarriorStoneCost() * modValue)) {
            canBuild = true;
        } else if (unitType.equals("troll") && resource.getMudQuantity() >= (unitProperties.getUglyTrollMudCost() * modValue)
                && resource.getStoneQuantity() >= (unitProperties.getUglyTrollStoneCost() * modValue)) {
            canBuild = true;
        }
        return canBuild;
    }

    public Unit modValOfGoblinArcher(Integer userId, Long gobQtyToMod){
        Unit unit = unitRepository.findUnitByUser_Id(userId);
        if (unit == null) {
            return null;
        }

        Long updatedGoblinQuantity = unit.getGoblinArcherQuantity()+gobQtyToMod;
        unit.setGoblinArcherQuantity(updatedGoblinQuantity < 0 ? 0 : updatedGoblinQuantity);

        return unitRepository.save(unit);
    }

    public Unit saveUnit(Unit unit) {
        return unitRepository.save(unit);
    }

    public Unit modValOfOrcWarrior(Integer userId, Long orcQtyToMod){
        Unit unit = unitRepository.findUnitByUser_Id(userId);
        if (unit == null) {
            return null;
        }

        Long updatedOrcQuantity = unit.getOrcWarriorQuantity()+orcQtyToMod;
        unit.setOrcWarriorQuantity(updatedOrcQuantity < 0 ? 0 : updatedOrcQuantity);

        return unitRepository.save(unit);
    }

    public Unit modValOfUglyTroll(Integer userId, Long trollQtyToMod){
        Unit unit = unitRepository.findUnitByUser_Id(userId);
        if (unit == null) {
            return null;
        }

        Long updatedTrollQuantity = unit.getUglyTrollQuantity()+trollQtyToMod;
        unit.setUglyTrollQuantity(updatedTrollQuantity < 0 ? 0 : updatedTrollQuantity);

        return unitRepository.save(unit);
    }
}

