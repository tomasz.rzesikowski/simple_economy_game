package pl.cm.project.simple_economy_gamebackend.unit.model;

import lombok.*;
import pl.cm.project.simple_economy_gamebackend.user.model.UserDto;

@EqualsAndHashCode
@ToString
@NoArgsConstructor
@Getter @Setter
public class UnitDto {

    private Long goblinArcherQuantity;

    private Long orcWarriorQuantity;

    private Long uglyTrollQuantity;

    private UserDto user;
}
