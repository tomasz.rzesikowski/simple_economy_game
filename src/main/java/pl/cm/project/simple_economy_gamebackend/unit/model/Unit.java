package pl.cm.project.simple_economy_gamebackend.unit.model;

import lombok.*;
import pl.cm.project.simple_economy_gamebackend.user.model.User;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@EqualsAndHashCode
@ToString
@NoArgsConstructor
@Table(name = "units")
public class Unit {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Getter
    private Integer id;

    @NotNull
    @Setter @Getter
    @Column(name = "goblin_archer_quantity")
    private Long goblinArcherQuantity;

    @NotNull
    @Setter @Getter
    @Column(name = "orc_warrior_quantity")
    private Long orcWarriorQuantity;

    @NotNull
    @Setter @Getter
    @Column(name = "ugly_troll_quantity")
    private Long uglyTrollQuantity;

    @Setter @Getter
    @OneToOne(mappedBy = "unit")
    private User user;
}
