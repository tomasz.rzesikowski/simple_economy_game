package pl.cm.project.simple_economy_gamebackend.unit.controller;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.cm.project.simple_economy_gamebackend.resource.service.ResourceService;
import pl.cm.project.simple_economy_gamebackend.unit.model.Unit;
import pl.cm.project.simple_economy_gamebackend.unit.model.UnitDto;
import pl.cm.project.simple_economy_gamebackend.unit.properties.UnitProperties;
import pl.cm.project.simple_economy_gamebackend.unit.service.UnitService;
import pl.cm.project.simple_economy_gamebackend.user.service.UserService;

@RestController
@RequestMapping(value = "/users/")
public class UnitController {
    private final UnitService unitService;

    private final UserService userService;

    private final ResourceService resourceService;

    private final UnitProperties unitProperties;

    private final ModelMapper modelMapper;

    @Autowired
    public UnitController(UnitService unitService, UserService userService, ResourceService resourceService, UnitProperties unitProperties, ModelMapper modelMapper) {
        this.unitService = unitService;
        this.userService = userService;
        this.resourceService = resourceService;
        this.unitProperties = unitProperties;
        this.modelMapper = modelMapper;
    }

    @GetMapping(value = "/{uid}/unit", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<UnitDto> getUnit(@PathVariable("uid") Integer uid) {
        if (!unitService.existsByUserId(uid)) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        if (!userService.isCurrentUser(uid)) {
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        }
        UnitDto unitDto = convertToDto(unitService.findUnitByUserId(uid));
        return new ResponseEntity<>(unitDto, HttpStatus.OK);
    }

    @PostMapping(value = "/{uid}/unit/mod", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<UnitDto> modUnit(@PathVariable("uid") Integer uid,
                                        @RequestParam("type") String unitType,
                                        @RequestParam("val") Long modValue) {
        if (!unitService.existsByUserId(uid)) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        if (!userService.isCurrentUser(uid)) {
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        }
        boolean canRecruit = unitService.canRecruitUnit(uid, unitType, modValue);
        if (canRecruit) {
            if (unitType.contains("gob")) {
                UnitDto unitDto = convertToDto(unitService.modValOfGoblinArcher(uid, modValue));
                resourceService.changeMudQuantity(uid, -(unitProperties.getGoblinArcherMudCost()) * modValue);
                return new ResponseEntity<>(unitDto, HttpStatus.OK);
            }
            if (unitType.contains("orc")) {
                UnitDto unitDto = convertToDto(unitService.modValOfOrcWarrior(uid, modValue));
                resourceService.changeStoneQuantity(uid, -(unitProperties.getOrcWarriorStoneCost()) * modValue);
                return new ResponseEntity<>(unitDto, HttpStatus.OK);
            }
            if (unitType.contains("troll")) {
                UnitDto unitDto = convertToDto(unitService.modValOfUglyTroll(uid, modValue));
                resourceService.changeMudQuantity(uid, -(unitProperties.getUglyTrollMudCost()) * modValue);
                resourceService.changeStoneQuantity(uid, -(unitProperties.getUglyTrollStoneCost()) * modValue);
                return new ResponseEntity<>(unitDto, HttpStatus.OK);
            }
        }
        return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }

    public UnitDto convertToDto(Unit unit) {
        return modelMapper.map(unit, UnitDto.class);
    }
}

