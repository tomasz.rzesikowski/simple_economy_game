package pl.cm.project.simple_economy_gamebackend.unit.model;

import pl.cm.project.simple_economy_gamebackend.user.model.User;

public final class UnitBuilder {
    private Long goblinArcherQuantity;
    private Long orcWarriorQuantity;
    private Long uglyTrollQuantity;
    private User user;

    private UnitBuilder() {
    }

    public static UnitBuilder anUnit() {
        return new UnitBuilder();
    }

    public UnitBuilder withGoblinArcherQuantity(Long goblinArcherQuantity) {
        this.goblinArcherQuantity = goblinArcherQuantity;
        return this;
    }

    public UnitBuilder withOrcWarriorQuantity(Long orcWarriorQuantity) {
        this.orcWarriorQuantity = orcWarriorQuantity;
        return this;
    }

    public UnitBuilder withUglyTrollQuantity(Long uglyTrollQuantity) {
        this.uglyTrollQuantity = uglyTrollQuantity;
        return this;
    }

    public UnitBuilder withUser(User user) {
        this.user = user;
        return this;
    }

    public Unit build() {
        Unit unit = new Unit();
        unit.setGoblinArcherQuantity(goblinArcherQuantity);
        unit.setOrcWarriorQuantity(orcWarriorQuantity);
        unit.setUglyTrollQuantity(uglyTrollQuantity);
        unit.setUser(user);
        return unit;
    }

    public Unit defaultBuild() {
        Unit unit = new Unit();
        unit.setGoblinArcherQuantity(0L);
        unit.setOrcWarriorQuantity(0L);
        unit.setUglyTrollQuantity(0L);
        unit.setUser(user);
        return unit;
    }
}
