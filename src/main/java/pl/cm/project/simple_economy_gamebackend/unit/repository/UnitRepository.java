package pl.cm.project.simple_economy_gamebackend.unit.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import pl.cm.project.simple_economy_gamebackend.unit.model.Unit;

@Transactional
@Repository
public interface UnitRepository extends JpaRepository<Unit, Integer> {
    Unit findUnitByUser_Id(Integer userId);
    boolean existsUnitByUser_Id(Integer userId);
}
