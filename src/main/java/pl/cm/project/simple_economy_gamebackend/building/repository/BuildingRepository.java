package pl.cm.project.simple_economy_gamebackend.building.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import pl.cm.project.simple_economy_gamebackend.building.model.Building;


@Transactional
@Repository
public interface BuildingRepository extends JpaRepository<Building, Integer> {

    Building findByUser_Id(Integer userId);
    boolean existsBuildingByUser_Id(Integer userId);
}
