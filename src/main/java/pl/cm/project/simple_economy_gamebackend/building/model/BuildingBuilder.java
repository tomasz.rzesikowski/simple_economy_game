package pl.cm.project.simple_economy_gamebackend.building.model;

import org.springframework.beans.factory.annotation.Autowired;
import pl.cm.project.simple_economy_gamebackend.building.properties.BuildingProperties;
import pl.cm.project.simple_economy_gamebackend.user.model.User;

public class BuildingBuilder {
    BuildingProperties buildingProperties;

    @Autowired
    public void setBuildingProperties(BuildingProperties buildingProperties) {
        this.buildingProperties = buildingProperties;
    }

    private Long mudGatherersCottage;
    private Long stoneQuarry;
    private Long huntersHut;
    private boolean goblinsCavern;
    private boolean orcsPit;
    private boolean trollsCave;
    private User user;

    private BuildingBuilder() {
    }

    public static BuildingBuilder aBuilding() {
        return new BuildingBuilder();
    }

    public BuildingBuilder withMudGatherersCottage(Long mudGatherersCottage) {
        this.mudGatherersCottage = mudGatherersCottage;
        return this;
    }

    public BuildingBuilder withStoneQuarry(Long stoneQuarry) {
        this.stoneQuarry = stoneQuarry;
        return this;
    }

    public BuildingBuilder withHuntersHut(Long huntersHut) {
        this.huntersHut = huntersHut;
        return this;
    }

    public BuildingBuilder withGoblinsCavern(boolean goblinsCavern) {
        this.goblinsCavern = goblinsCavern;
        return this;
    }

    public BuildingBuilder withOrcsPit(boolean orcsPit) {
        this.orcsPit = orcsPit;
        return this;
    }

    public BuildingBuilder withTrollsCave(boolean trollsCave) {
        this.trollsCave = trollsCave;
        return this;
    }

    public BuildingBuilder withUser(User user) {
        this.user = user;
        return this;
    }

    public Building build() {
        Building building = new Building();
        building.setMudGatherersCottageQuantity(mudGatherersCottage);
        building.setStoneQuarryQuantity(stoneQuarry);
        building.setHuntersHutQuantity(huntersHut);
        building.setGoblinsCavernOwnership(goblinsCavern);
        building.setOrcsPitOwnership(orcsPit);
        building.setTrollsCaveOwnership(trollsCave);
        building.setUser(user);
        return building;
    }

    public Building defaultBuild() {
        Building building = new Building();
        building.setMudGatherersCottageQuantity(0L);
        building.setStoneQuarryQuantity(0L);
        building.setHuntersHutQuantity(0L);
        building.setGoblinsCavernOwnership(false);
        building.setOrcsPitOwnership(false);
        building.setTrollsCaveOwnership(false);
        building.setUser(user);
        return building;
    }
}
