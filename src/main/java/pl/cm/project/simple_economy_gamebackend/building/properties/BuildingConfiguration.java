package pl.cm.project.simple_economy_gamebackend.building.properties;

import org.springframework.beans.factory.config.PropertiesFactoryBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.Properties;

@Configuration
@RestController
class BuildingConfiguration {

    @Bean(name = "buildingsProperties")
    public static PropertiesFactoryBean mapper() {
        PropertiesFactoryBean bean = new PropertiesFactoryBean();
        bean.setLocation(new ClassPathResource("building.properties"));
        return bean;
    }

    @Resource(name="buildingsProperties")
    private final Properties building = new Properties();

    public Properties getBuilding() {
        return building;
    }

    @GetMapping(value = "/bud", produces = {MediaType.APPLICATION_JSON_VALUE})
    ResponseEntity<Object> getBuildingCost() {
        final StringBuilder string = new StringBuilder("[");
        getBuilding().forEach((k,v)-> string.append("{\"name\":\"").append(k).append("\",\"value\":\"").append(v).append("\"},"));
        string.setLength(string.length() - 1);
        string.append("]");
        String newString = string.toString();
        String replacedString = newString.replaceAll("\\bbuilding.\\b", "");
        newString = replacedString.replaceAll("_",  "\\ ");
        replacedString  = newString.replaceAll("\\b mud\\b",  "\",\"type\":\"mud");
        newString = replacedString .replaceAll("\\b stone\\b",  "\",\"type\":\"stone");
        return new ResponseEntity<>(newString, HttpStatus.OK);
    }
}
