package pl.cm.project.simple_economy_gamebackend.building.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.cm.project.simple_economy_gamebackend.building.model.Building;
import pl.cm.project.simple_economy_gamebackend.building.properties.BuildingProperties;
import pl.cm.project.simple_economy_gamebackend.building.repository.BuildingRepository;
import pl.cm.project.simple_economy_gamebackend.resource.model.Resource;
import pl.cm.project.simple_economy_gamebackend.resource.service.ResourceService;

import java.util.List;

@Service
public class BuildingService {

    private BuildingRepository buildingRepository;

    private ResourceService resourceService;

    private BuildingProperties buildingProperties;

    @Autowired
    public void setBuildingRepository(BuildingRepository buildingRepository) {
        this.buildingRepository = buildingRepository;
    }

    @Autowired
    public void setResourceService(ResourceService resourceService) {
        this.resourceService = resourceService;
    }

    @Autowired
    public void setBuildingProperties(BuildingProperties buildingProperties) {
        this.buildingProperties = buildingProperties;
    }

    public Building findBuildingByUser_Id(Integer userId){
        return buildingRepository.findByUser_Id(userId);
    }

    public boolean existsByUser_Id(Integer userId){
        return buildingRepository.existsBuildingByUser_Id(userId);
    }

    public boolean canBuildMatBuilding(Integer userId, String buildingType, Long modValue) {
        Resource resource = resourceService.findResourceByUserId(userId);
        boolean canBuild = false;
        if (buildingType.equals("mud") && modValue > 0
                && resource.getMudQuantity() >= (buildingProperties.getMudGatherersCottageMudCost() * modValue)) {
                canBuild = true;
        } else if (buildingType.equals("sto") && modValue > 0
                && resource.getMudQuantity() >= buildingProperties.getStoneQuarryMudCost() * modValue) {
                canBuild = true;
        } else if (buildingType.equals("hun") && modValue > 0
                && resource.getMudQuantity() >= buildingProperties.getHuntersHutMudCost() * modValue
                &&  resource.getStoneQuantity() >= buildingProperties.getHuntersHutStoneCost() * modValue) {
                canBuild = true;
        }
        return canBuild;
    }

    public boolean canBuildResBuilding(Integer userId, String buildingType) {
        Building building = buildingRepository.findByUser_Id(userId);
        Resource resource = resourceService.findResourceByUserId(userId);
        boolean canBuild = false;
        if (buildingType.equals("gob") && resource.getMudQuantity() >= buildingProperties.getGoblinsCavernMudCost()
                    && resource.getStoneQuantity() >= buildingProperties.getGoblinsCavernStoneCost()
                    && !building.isGoblinsCavernOwnership()) {
                canBuild = true;
        } else if (buildingType.equals("orc") && resource.getMudQuantity() >= buildingProperties.getOrcsPitMudCost()
                    && resource.getStoneQuantity() >= buildingProperties.getOrcsPitStoneCost()
                    && building.isGoblinsCavernOwnership()
                    && !building.isOrcsPitOwnership()) {
                canBuild = true;
        } else if (buildingType.equals("tro") && resource.getMudQuantity() >= buildingProperties.getTrollsCaveMudCost()
                    && resource.getStoneQuantity() >= buildingProperties.getTrollsCaveStoneCost()
                    && building.isOrcsPitOwnership()
                    && !building.isTrollsCaveOwnership()) {
                canBuild = true;
        }
        return canBuild;
    }

    public Building modValOfMudGatherersCottage(Integer userId, Long QtyToMod){
        Building building = buildingRepository.findByUser_Id(userId);
        if (building == null) {
            return null;
        }

        Long updatedMudGatherersCottageQuantity = building.getMudGatherersCottageQuantity() + QtyToMod;
        building.setMudGatherersCottageQuantity(updatedMudGatherersCottageQuantity);

        return buildingRepository.save(building);
    }

    public Building modValOfStoneQuarry(Integer userId, Long QtyToMod){
        Building building = buildingRepository.findByUser_Id(userId);
        if (building == null) {
            return null;
        }

        Long updatedStoneQuarryQuantity = building.getStoneQuarryQuantity() + QtyToMod;
        building.setStoneQuarryQuantity(updatedStoneQuarryQuantity);

        return buildingRepository.save(building);
    }

    public Building modValOfHuntersHut(Integer userId, Long QtyToMod){
        Building building = buildingRepository.findByUser_Id(userId);
        if (building == null) {
            return null;
        }

        Long updatedHuntersHutQuantity = building.getHuntersHutQuantity() + QtyToMod;
        building.setHuntersHutQuantity(updatedHuntersHutQuantity);

        return buildingRepository.save(building);
    }

    public Building modValOfOrcsPit(Integer userId){
        Building building = buildingRepository.findByUser_Id(userId);
        if (building == null) {
            return null;
        }
        building.setOrcsPitOwnership(true);

        return buildingRepository.save(building);
    }

    public Building modValOfGoblinsCavern(Integer userId){
        Building building = buildingRepository.findByUser_Id(userId);
        if (building == null) {
            return null;
        }
        building.setGoblinsCavernOwnership(true);

        return buildingRepository.save(building);
    }

    public Building modValOfTrollsCave(Integer userId){
        Building building = buildingRepository.findByUser_Id(userId);
        if (building == null) {
            return null;
        }
        building.setTrollsCaveOwnership(true);

        return buildingRepository.save(building);
    }

    public List<Building> findAllBuildings() {
        return buildingRepository.findAll();
    }
}
