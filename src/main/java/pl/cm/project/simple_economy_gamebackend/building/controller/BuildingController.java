package pl.cm.project.simple_economy_gamebackend.building.controller;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.cm.project.simple_economy_gamebackend.building.model.Building;
import pl.cm.project.simple_economy_gamebackend.building.model.BuildingDto;
import pl.cm.project.simple_economy_gamebackend.building.properties.BuildingProperties;
import pl.cm.project.simple_economy_gamebackend.building.service.BuildingService;
import pl.cm.project.simple_economy_gamebackend.resource.service.ResourceService;
import pl.cm.project.simple_economy_gamebackend.user.service.UserService;

@RestController
@RequestMapping("/users/")
public class BuildingController {

    private final BuildingService buildingService;
    private final UserService userService;
    private final ResourceService resourceService;
    private final BuildingProperties buildingProperties;
    private final ModelMapper modelMapper;

    @Autowired
    public BuildingController(BuildingService buildingService, UserService userService, ResourceService resourceService,
                              BuildingProperties buildingProperties, ModelMapper modelMapper) {
        this.buildingService = buildingService;
        this.userService = userService;
        this.resourceService = resourceService;
        this.buildingProperties = buildingProperties;
        this.modelMapper = modelMapper;
    }

    @GetMapping(value = "/{uid}/building", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<BuildingDto> getBuilding(@PathVariable("uid") Integer uid) {
        if (!buildingService.existsByUser_Id(uid)) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        if (!userService.isCurrentUser(uid)) {
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        }
        BuildingDto buildingDto = convertToDto(buildingService.findBuildingByUser_Id(uid));
        return new ResponseEntity<>(buildingDto, HttpStatus.OK);
    }

    @PostMapping(value = "/{uid}/building/modmat", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Building> modMatBuilding(@PathVariable("uid") Integer uid,
                                                   @RequestParam("bud") String buildingType,
                                                   @RequestParam("val") Long modValue) {
        if (!buildingService.existsByUser_Id(uid)) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        if (!userService.isCurrentUser(uid)) {
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        }
        boolean canBuild = buildingService.canBuildMatBuilding(uid, buildingType, modValue);
        if (canBuild) {
            if (buildingType.contains("mud")) {
                Building building = buildingService.modValOfMudGatherersCottage(uid, modValue);
                resourceService.changeMudQuantity(uid, -(buildingProperties.getMudGatherersCottageMudCost()) * modValue);
                return new ResponseEntity<>(building, HttpStatus.OK);
            } else if (buildingType.contains("sto")) {
                Building building = buildingService.modValOfStoneQuarry(uid, modValue);
                resourceService.changeMudQuantity(uid, -(buildingProperties.getStoneQuarryMudCost()) * modValue);
                return new ResponseEntity<>(building, HttpStatus.OK);
            } else if (buildingType.contains("hun")) {
                Building building = buildingService.modValOfHuntersHut(uid, modValue);
                resourceService.changeMudQuantity(uid, -(buildingProperties.getHuntersHutMudCost()) * modValue);
                resourceService.changeStoneQuantity(uid, -(buildingProperties.getHuntersHutStoneCost()) * modValue);
                return new ResponseEntity<>(building, HttpStatus.OK);
            }
        }
        return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }

    @PostMapping(value = "/{uid}/building/modrec", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Building> modRecBuilding(@PathVariable("uid") Integer uid,
                                        @RequestParam("bud") String buildingType,
                                        @RequestParam("val") boolean isBuild) {
        if (!buildingService.existsByUser_Id(uid)) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        if (!userService.isCurrentUser(uid)) {
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        }
        boolean canBuild = buildingService.canBuildResBuilding(uid, buildingType);
        if (canBuild) {
            if (buildingType.contains("gob")) {
                Building building = buildingService.modValOfGoblinsCavern(uid);
                resourceService.changeMudQuantity(uid, -buildingProperties.getGoblinsCavernMudCost());
                resourceService.changeStoneQuantity(uid, -buildingProperties.getGoblinsCavernStoneCost());
                return new ResponseEntity<>(building, HttpStatus.OK);
            } else if (buildingType.contains("orc")) {
                Building building = buildingService.modValOfOrcsPit(uid);
                resourceService.changeMudQuantity(uid, -buildingProperties.getOrcsPitMudCost());
                resourceService.changeStoneQuantity(uid, -buildingProperties.getOrcsPitStoneCost());
                return new ResponseEntity<>(building, HttpStatus.OK);
            } else if (buildingType.contains("tro")) {
                Building building = buildingService.modValOfTrollsCave(uid);
                resourceService.changeMudQuantity(uid, -buildingProperties.getTrollsCaveMudCost());
                resourceService.changeStoneQuantity(uid, -buildingProperties.getTrollsCaveStoneCost());
                return new ResponseEntity<>(building, HttpStatus.OK);
            }
        }
        return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }

    public BuildingDto convertToDto(Building building) {
        return modelMapper.map(building, BuildingDto.class);
    }
}
