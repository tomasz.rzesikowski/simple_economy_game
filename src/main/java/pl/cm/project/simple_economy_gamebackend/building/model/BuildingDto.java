package pl.cm.project.simple_economy_gamebackend.building.model;

import lombok.*;
import pl.cm.project.simple_economy_gamebackend.user.model.UserDto;

@EqualsAndHashCode
@ToString
@NoArgsConstructor
@Getter @Setter
public class BuildingDto {

    private Long mudGatherersCottageQuantity;

    private Long stoneQuarryQuantity;

    private Long huntersHutQuantity;

    private boolean goblinsCavernOwnership;

    private boolean orcsPitOwnership;

    private boolean trollsCaveOwnership;

    private UserDto user;
}


