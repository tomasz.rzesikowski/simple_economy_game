package pl.cm.project.simple_economy_gamebackend.building.model;

import lombok.*;
import pl.cm.project.simple_economy_gamebackend.user.model.User;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@EqualsAndHashCode
@ToString
@NoArgsConstructor
@Table(name = "buildings")
public class Building {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Getter
    private Integer id;

    @NotNull
    @Setter @Getter
    @Column(name = "mud_gatherers_cottage_quantity")
    private Long mudGatherersCottageQuantity;

    @NotNull
    @Setter @Getter
    @Column(name = "stone_quarry_quantity")
    private Long stoneQuarryQuantity;

    @NotNull
    @Setter @Getter
    @Column(name = "hunters_hut_quantity")
    private Long huntersHutQuantity;

    @NotNull
    @Setter @Getter
    @Column(name = "goblins_caverns_ownership")
    private boolean goblinsCavernOwnership;

    @NotNull
    @Setter @Getter
    @Column(name = "orcs_pit_ownership")
    private boolean orcsPitOwnership;

    @NotNull
    @Setter @Getter
    @Column(name = "trolls_cave_ownership")
    private boolean trollsCaveOwnership;

    @Setter @Getter
    @OneToOne(mappedBy = "building")
    private User user;
}
