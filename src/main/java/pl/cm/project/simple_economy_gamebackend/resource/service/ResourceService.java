package pl.cm.project.simple_economy_gamebackend.resource.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.cm.project.simple_economy_gamebackend.resource.model.Resource;
import pl.cm.project.simple_economy_gamebackend.resource.repository.ResourceRepository;

@Service
public class ResourceService {

    private ResourceRepository resourceRepository;

    @Autowired
    public void setResourceRepository(ResourceRepository resourceRepository) {
        this.resourceRepository = resourceRepository;
    }

    public Resource saveResource(Resource resource) {
        return resourceRepository.save(resource);
    }

    public Resource findResourceByUserId(Integer userId) {
        return resourceRepository.findByUserId(userId);
    }

    public boolean existsByUserId(Integer userId) {
        return resourceRepository.existsByUser_Id(userId);
    }

    public void changeMudQuantity(Integer userId, Long mudQtyToAdd) {
        Resource resource = resourceRepository.findByUserId(userId);
        if (resource.getMudQuantity() + mudQtyToAdd < 0) {
            resource.setMudQuantity(0L);
        } else {
            resource.setMudQuantity(resource.getMudQuantity() + mudQtyToAdd);
        }
        resourceRepository.save(resource);
    }


    public void changeStoneQuantity(Integer userId, Long stoneQtyToAdd) {
        Resource resource = resourceRepository.findByUserId(userId);
        if (resource.getStoneQuantity() + stoneQtyToAdd < 0) {
            resource.setStoneQuantity(0L);
        } else {
            resource.setStoneQuantity(resource.getStoneQuantity() + stoneQtyToAdd);
        }
        resourceRepository.save(resource);
    }

    public void changeMeatQuantity(Integer userId, Long meatQtyToAdd) {
        Resource resource = resourceRepository.findByUserId(userId);
        if (resource.getMeatQuantity() + meatQtyToAdd < 0) {
            resource.setMeatQuantity(0L);
        } else {
            resource.setMeatQuantity(resource.getMeatQuantity() + meatQtyToAdd);
        }
        resourceRepository.save(resource);
    }

}
