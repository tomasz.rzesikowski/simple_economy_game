package pl.cm.project.simple_economy_gamebackend.resource.controller;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pl.cm.project.simple_economy_gamebackend.resource.model.Resource;
import pl.cm.project.simple_economy_gamebackend.resource.model.ResourceDto;
import pl.cm.project.simple_economy_gamebackend.resource.service.ResourceService;
import pl.cm.project.simple_economy_gamebackend.user.service.UserService;

@RestController
@RequestMapping("/users")
public class ResourceController {

    private final ResourceService resourceService;
    private final UserService userService;
    private final ModelMapper modelMapper;

    @Autowired
    public ResourceController(ResourceService resourceService, UserService userService, ModelMapper modelMapper) {
        this.resourceService = resourceService;
        this.userService = userService;
        this.modelMapper = modelMapper;
    }

    @GetMapping(value = "/{uid}/resources", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ResourceDto> getResource(@PathVariable("uid") Integer uid) {
        if (!resourceService.existsByUserId(uid)) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        if (!userService.isCurrentUser(uid)) {
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        }
        ResourceDto resourceDto = convertToDto(resourceService.findResourceByUserId(uid));
        return new ResponseEntity<>(resourceDto, HttpStatus.OK);
    }

    public ResourceDto convertToDto(Resource resource) {
        return modelMapper.map(resource, ResourceDto.class);
    }
}
