package pl.cm.project.simple_economy_gamebackend.resource.model;

import lombok.*;
import pl.cm.project.simple_economy_gamebackend.user.model.User;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
@EqualsAndHashCode
@ToString
@NoArgsConstructor
public class Resource {

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Getter
    private Integer id;

    @NotNull
    @Setter @Getter
    @Column(name = "mud_quantity")
    private Long mudQuantity;

    @NotNull
    @Setter @Getter
    @Column(name = "stone_quantity")
    private Long stoneQuantity;

    @NotNull
    @Setter @Getter
    @Column(name = "meat_quantity")
    private Long meatQuantity;

    @Setter @Getter
    @OneToOne(mappedBy = "resource")
    private User user;

}
