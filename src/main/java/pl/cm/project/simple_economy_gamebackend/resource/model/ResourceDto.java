package pl.cm.project.simple_economy_gamebackend.resource.model;

import lombok.*;
import pl.cm.project.simple_economy_gamebackend.user.model.UserDto;

@EqualsAndHashCode
@ToString
@NoArgsConstructor
@Getter @Setter
public class ResourceDto {

    private Long mudQuantity;

    private Long stoneQuantity;

    private Long meatQuantity;

    private UserDto user;
}
