package pl.cm.project.simple_economy_gamebackend.resource.model;

import pl.cm.project.simple_economy_gamebackend.user.model.User;

public class ResourceBuilder {
    private Long mudQuantity;
    private Long stoneQuantity;
    private Long meatQuantity;
    private User user;

    private ResourceBuilder() {
    }

    public static ResourceBuilder aResource() {
        return new ResourceBuilder();
    }

    public ResourceBuilder withMudQuantity(Long mudQuantity) {
        this.mudQuantity = mudQuantity;
        return this;
    }

    public ResourceBuilder withStoneQuantity(Long stoneQuantity) {
        this.stoneQuantity = stoneQuantity;
        return this;
    }

    public ResourceBuilder withMeatQuantity(Long meatQuantity) {
        this.meatQuantity = meatQuantity;
        return this;
    }

    public ResourceBuilder withUser(User user) {
        this.user = user;
        return this;
    }

    public Resource build() {
        Resource resource = new Resource();
        resource.setMudQuantity(mudQuantity);
        resource.setStoneQuantity(stoneQuantity);
        resource.setMeatQuantity(meatQuantity);
        resource.setUser(user);
        return resource;
    }

    public Resource defaultBuild() {
        Resource resource = new Resource();
        resource.setMudQuantity(100L);
        resource.setStoneQuantity(0L);
        resource.setMeatQuantity(0L);
        resource.setUser(user);
        return resource;
    }
}
