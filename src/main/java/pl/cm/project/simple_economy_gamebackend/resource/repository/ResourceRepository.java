package pl.cm.project.simple_economy_gamebackend.resource.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import pl.cm.project.simple_economy_gamebackend.resource.model.Resource;

@Repository
@Transactional
public interface ResourceRepository extends JpaRepository<Resource, Integer> {

    Resource findByUserId(Integer userId);

    boolean existsByUser_Id(Integer userId);

}
