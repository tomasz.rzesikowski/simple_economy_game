package pl.cm.project.simple_economy_gamebackend;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import pl.cm.project.simple_economy_gamebackend.building.model.Building;
import pl.cm.project.simple_economy_gamebackend.building.properties.BuildingProperties;
import pl.cm.project.simple_economy_gamebackend.building.service.BuildingService;
import pl.cm.project.simple_economy_gamebackend.resource.model.Resource;
import pl.cm.project.simple_economy_gamebackend.resource.service.ResourceService;
import pl.cm.project.simple_economy_gamebackend.unit.model.Unit;
import pl.cm.project.simple_economy_gamebackend.unit.properties.UnitProperties;
import pl.cm.project.simple_economy_gamebackend.unit.service.UnitService;

import java.util.List;

@Configuration
@EnableScheduling
public class ScheduleConfig {
    BuildingService buildingService;
    ResourceService resourceService;
    UnitService unitService;
    UnitProperties unitProperties;
    BuildingProperties buildingProperties;

    @Autowired
    public void setBuildingService(BuildingService buildingService) {
        this.buildingService = buildingService;
    }

    @Autowired
    public void setUnitService(UnitService unitService) {
        this.unitService = unitService;
    }

    @Autowired
    public void setResourceService(ResourceService resourceService) {
        this.resourceService = resourceService;
    }

    @Autowired
    public void setUnitProperties(UnitProperties unitProperties) {
        this.unitProperties = unitProperties;
    }

    @Autowired
    public void setBuildingProperties(BuildingProperties buildingProperties) {
        this.buildingProperties = buildingProperties;
    }

    @Scheduled(fixedDelay = 1000)
    public void scheduleFixedDelayTask() {
        int scheduleDelayInSeconds = 1000/100;
        List<Building> allBuildings = buildingService.findAllBuildings();
        for (Building building: allBuildings) {
            Integer userId = building.getUser().getId();
            Resource resource = resourceService.findResourceByUserId(userId);
            Unit unit = unitService.findUnitByUserId(userId);

            Long mud = resource.getMudQuantity();
            Long stone = resource.getStoneQuantity();
            Long meat = resource.getMeatQuantity();

            Long mudProd = building.getMudGatherersCottageQuantity() * buildingProperties.getMudGatherersCottageProd();
            resource.setMudQuantity(mud + mudProd);

            Long stoneProd = building.getStoneQuarryQuantity() * buildingProperties.getStoneQuarryProd();
            resource.setStoneQuantity(stone + stoneProd);

            Long meatCost = (unit.getGoblinArcherQuantity() * unitProperties.getGoblinArcherMeatCost()
                    + unit.getOrcWarriorQuantity() * unitProperties.getOrcWarriorMeatCost()
                    + unit.getUglyTrollQuantity() * unitProperties.getGoblinArcherMeatCost())
                    * scheduleDelayInSeconds;

            long meatProd = building.getHuntersHutQuantity() * buildingProperties.getHuntersHutProd();
            long meatSum = meat - meatCost + meatProd;
            meatSum = desertion(unit, meatSum);
            resource.setMeatQuantity(meatSum);

            resourceService.saveResource(resource);
        }

    }
    private long desertion(Unit unit, Long meatSum) {
        while (meatSum < 0) {
            if (unit.getUglyTrollQuantity() > 0) {
                unit.setUglyTrollQuantity(unit.getUglyTrollQuantity() - 1);
                meatSum += unitProperties.getUglyTrollMeatCost();
                continue;
            }
            if (unit.getOrcWarriorQuantity() > 0) {
                unit.setOrcWarriorQuantity(unit.getOrcWarriorQuantity() - 1);
                meatSum += unitProperties.getOrcWarriorMeatCost();
                continue;
            }

            unit.setGoblinArcherQuantity(unit.getGoblinArcherQuantity() - 1);
            meatSum += unitProperties.getGoblinArcherMeatCost();
        }
        unitService.saveUnit(unit);
        return meatSum;
    }
}
