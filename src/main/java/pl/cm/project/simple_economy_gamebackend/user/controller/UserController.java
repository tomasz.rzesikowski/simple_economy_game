package pl.cm.project.simple_economy_gamebackend.user.controller;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;
import pl.cm.project.simple_economy_gamebackend.building.model.Building;
import pl.cm.project.simple_economy_gamebackend.building.model.BuildingBuilder;
import pl.cm.project.simple_economy_gamebackend.resource.model.Resource;
import pl.cm.project.simple_economy_gamebackend.resource.model.ResourceBuilder;
import pl.cm.project.simple_economy_gamebackend.unit.model.Unit;
import pl.cm.project.simple_economy_gamebackend.unit.model.UnitBuilder;
import pl.cm.project.simple_economy_gamebackend.user.model.User;
import pl.cm.project.simple_economy_gamebackend.user.model.UserDto;
import pl.cm.project.simple_economy_gamebackend.user.model.UserStatusDTO;
import pl.cm.project.simple_economy_gamebackend.user.service.UserService;

import javax.validation.ConstraintViolationException;
import java.util.List;
import java.util.stream.Collectors;

@RestController
public class UserController {
    private final UserService userService;

    private final ModelMapper modelMapper;

    @Autowired
    public UserController(UserService userService, ModelMapper modelMapper) {
        this.userService = userService;
        this.modelMapper = modelMapper;
    }

    @PostMapping(value = "/add", consumes = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<Object> addUser(@RequestBody UserDto userDto, UriComponentsBuilder uri) {
        User user = convertToEntity(userDto);
        try {
            Building startBuilding = BuildingBuilder.aBuilding().defaultBuild();
            Unit startUnit = UnitBuilder.anUnit().defaultBuild();
            Resource startResource = ResourceBuilder.aResource().defaultBuild();

            user.setBuilding(startBuilding);
            user.setUnit(startUnit);
            user.setResource(startResource);
            user = userService.addUser(user);
        } catch (ConstraintViolationException e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(uri.path("users/{id}").buildAndExpand(user.getId()).toUri());
        return new ResponseEntity<>(headers, HttpStatus.CREATED);
    }

    @DeleteMapping(value = "/admin/{id}")
    public ResponseEntity<HttpStatus> removeUser(@PathVariable("id") int id) {
        if (userService.userWithIdExists(id)) {
            userService.deleteUserById(id);
            return new ResponseEntity<>(HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @GetMapping(value = "/admin/all", produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<List<UserDto>> allUsers(@RequestHeader(defaultValue = MediaType.APPLICATION_JSON_VALUE, value = "Accept") String accept) {

        if (accept.contains(MediaType.APPLICATION_JSON_VALUE)) {
            List<User> users = userService.findAllUsers();
            List<UserDto> usersDto = users.stream().map(this::convertToDto).collect(Collectors.toList());
            return new ResponseEntity<List<UserDto>>(usersDto, HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.NOT_ACCEPTABLE);
    }

    @GetMapping(value = "/login", produces = {MediaType.APPLICATION_JSON_VALUE})
    public UserStatusDTO validateLogin(){
        return new UserStatusDTO("User authenticated");
    }

    public UserDto convertToDto(User user) {
        return modelMapper.map(user, UserDto.class);
    }

    public User convertToEntity(UserDto userDto) {
        return modelMapper.map(userDto, User.class);
    }
}
