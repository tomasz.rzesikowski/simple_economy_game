package pl.cm.project.simple_economy_gamebackend.user.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import pl.cm.project.simple_economy_gamebackend.user.model.SecurityUser;
import pl.cm.project.simple_economy_gamebackend.user.model.User;
import pl.cm.project.simple_economy_gamebackend.user.repository.UserRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.springframework.security.core.context.SecurityContextHolder.getContext;

@Service
public class UserService {

    private UserRepository userRepository;

    @Autowired
    public void setUserRepository(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public User addUser(User user){
        String encodePassword = new BCryptPasswordEncoder().encode(user.getPassword());
        user.setPassword(encodePassword);
        return userRepository.save(user);
    }
    public Optional<User> findUserById (Integer id) {
        return userRepository.findById(id);
    }
    public void deleteUserById(Integer id) {
        userRepository.deleteById(id);
    }
    public boolean userWithIdExists(Integer id) {
        return userRepository.existsById(id);
    }
    public boolean userWithNameExists(String username) {
        return userRepository.existsByUsername(username);
    }
    public SecurityUser getSecurityUser(String username) throws UsernameNotFoundException{
        if (!this.userWithNameExists(username)) {
            throw new UsernameNotFoundException("User not found");
        }

        User user = userRepository.findByUsername(username);

        String[] rolesArray = user.getRoles().split(",");
        List<SimpleGrantedAuthority> authorityRoles = new ArrayList<>();
        for (String s : rolesArray) {
            authorityRoles.add(new SimpleGrantedAuthority(s));
        }

        return new SecurityUser(
                user.getUsername(),
                user.getPassword(),
                authorityRoles
        );
    }
    public boolean isCurrentUser(Integer id) {
        Optional<User> foundedUser = userRepository.findById(id);
        if (foundedUser.isEmpty()) {
            return false;
        }
        String username = getContext().getAuthentication().getName();
        return username.matches(foundedUser.get().getUsername());
    }
    public List<User> findAllUsers() {
        return userRepository.findAll();
    }
}
