package pl.cm.project.simple_economy_gamebackend.user.model;

public class UserStatusDTO {
    private String status;

    public UserStatusDTO(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
