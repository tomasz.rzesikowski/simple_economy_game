package pl.cm.project.simple_economy_gamebackend.user.model;

import pl.cm.project.simple_economy_gamebackend.building.model.Building;
import pl.cm.project.simple_economy_gamebackend.resource.model.Resource;
import pl.cm.project.simple_economy_gamebackend.unit.model.Unit;

public final class UserBuilder {
    private String username;
    private String password;
    private String roles;
    private Resource resource;
    private Unit unit;
    private Building building;

    private UserBuilder() {
    }

    public static UserBuilder anUser() {
        return new UserBuilder();
    }

    public UserBuilder withUsername(String username) {
        this.username = username;
        return this;
    }

    public UserBuilder withPassword(String password) {
        this.password = password;
        return this;
    }

    public UserBuilder withRoles(String roles) {
        this.roles = roles;
        return this;
    }

    public UserBuilder withResource(Resource resource) {
        this.resource = resource;
        return this;
    }

    public UserBuilder withUnit(Unit unit) {
        this.unit = unit;
        return this;
    }

    public UserBuilder withBuilding(Building building) {
        this.building = building;
        return this;
    }

    public User build() {
        User user = new User();
        user.setUsername(username);
        user.setPassword(password);
        user.setRoles(roles);
        user.setResource(resource);
        user.setUnit(unit);
        user.setBuilding(building);
        return user;
    }
}
