package pl.cm.project.simple_economy_gamebackend.user.model;

import lombok.*;
import pl.cm.project.simple_economy_gamebackend.building.model.Building;
import pl.cm.project.simple_economy_gamebackend.resource.model.Resource;
import pl.cm.project.simple_economy_gamebackend.unit.model.Unit;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@NoArgsConstructor
@EqualsAndHashCode
@ToString
@Entity
@Table(name = "users")
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Getter
    private Integer id;

    @Column(unique = true)
    @NotNull
    @Setter @Getter
    private String username;

    @NotNull
    @Setter @Getter
    private String password;

    @NotNull
    @Getter @Setter
    private String roles;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "resource_id", referencedColumnName = "id")
    @Getter @Setter
    private Resource resource;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "unit_id", referencedColumnName = "id")
    @Getter @Setter
    private Unit unit;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "building_id", referencedColumnName = "id")
    @Getter @Setter
    private Building building;

}
