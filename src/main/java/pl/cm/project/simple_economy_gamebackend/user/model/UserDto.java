package pl.cm.project.simple_economy_gamebackend.user.model;

import lombok.*;

@NoArgsConstructor
@EqualsAndHashCode
@ToString
@Getter @Setter
public class UserDto {

    private Integer id;

    private String username;

    private String password;

    private String roles;
}
