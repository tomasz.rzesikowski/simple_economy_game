package pl.cm.project.simple_economy_gamebackend.building.service;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import pl.cm.project.simple_economy_gamebackend.building.model.Building;
import pl.cm.project.simple_economy_gamebackend.building.properties.BuildingProperties;
import pl.cm.project.simple_economy_gamebackend.building.repository.BuildingRepository;
import pl.cm.project.simple_economy_gamebackend.resource.service.ResourceService;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)
public class BuildingServiceTest {

    @TestConfiguration
    static class ServiceTestContextConfiguration {

        @Bean
        public BuildingService buildingService() {
            return new BuildingService();
        }
    }

    @Autowired
    private BuildingService buildingService;

    @MockBean
    private BuildingRepository buildingRepository;

    @MockBean
    private ResourceService resourceService;

    @MockBean
    private BuildingProperties buildingProperties;

    @Test
    void givenNoExistedUserId_whenExistByUserID_thenFalse() {
        when(buildingRepository.existsBuildingByUser_Id(1)).thenReturn(false);

        boolean isExisted = buildingService.existsByUser_Id(1);

        assertFalse(isExisted);
    }

    @Test
    void givenNoExistedUserIdAndMudGatherersCottageQtyGreater_whenModValOfMudGatherersCottage_thenNull() {
        Long qtyToMod = -100L;
        Integer notExistedId = 1;
        when(buildingRepository.findByUser_Id(notExistedId)).thenReturn(null);
        Building responseBuilding = buildingService.modValOfMudGatherersCottage(notExistedId, qtyToMod);
        assertNull(responseBuilding);
    }

    @Test
    void givenExistedUserId_whenExistByUserID_thenTrue() {
        when(buildingRepository.existsBuildingByUser_Id(1)).thenReturn(true);

        boolean isExisted = buildingService.existsByUser_Id(1);

        assertTrue(isExisted);
    }
}
