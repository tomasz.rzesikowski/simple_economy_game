package pl.cm.project.simple_economy_gamebackend.building.controller;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import pl.cm.project.simple_economy_gamebackend.building.model.Building;
import pl.cm.project.simple_economy_gamebackend.building.model.BuildingBuilder;
import pl.cm.project.simple_economy_gamebackend.building.properties.BuildingProperties;
import pl.cm.project.simple_economy_gamebackend.building.service.BuildingService;
import pl.cm.project.simple_economy_gamebackend.resource.service.ResourceService;
import pl.cm.project.simple_economy_gamebackend.user.model.User;
import pl.cm.project.simple_economy_gamebackend.user.model.UserBuilder;
import pl.cm.project.simple_economy_gamebackend.user.service.UserService;


import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


@ExtendWith(SpringExtension.class)
@WebMvcTest(BuildingController.class)
@WithMockUser(username = "tester")
public class BuildingControllerTest {

    @Autowired
    private MockMvc mvc;
    @MockBean
    private BuildingService buildingService;
    @MockBean
    private UserService userService;
    @MockBean
    private ResourceService resourceService;
    @MockBean
    private BuildingProperties buildingProperties;

    @Test
    @WithMockUser(username = "tester")
    public void givenUserAndBuilding_whenGetBuilding_thenReturnResourceDto() throws Exception {

        User testUser = UserBuilder
                .anUser()
                .withUsername("tester")
                .withPassword("tester")
                .withRoles("USER")
                .build();

        Building testBuilding = BuildingBuilder
                .aBuilding()
                .withHuntersHut(1000L)
                .withUser(testUser)
                .build();

        when(buildingService.existsByUser_Id(1)).thenReturn(true);
        when(buildingService.findBuildingByUser_Id(1)).thenReturn(testBuilding);
        when(userService.isCurrentUser(1)).thenReturn(true);

        mvc.perform(get("/users/1/building")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("huntersHutQuantity").value(testBuilding.getHuntersHutQuantity()));
    }

    @Test
    public void givenNoExistedUserId_whenGetBuilding_thenNotFound() throws  Exception{
        when(buildingService.existsByUser_Id(1)).thenReturn(false);

        mvc.perform(get("/users/1/building"))
                .andExpect(status().isNotFound());
    }

    @Test
    void givenWrongRequestToModMatBuilding_whenModMatBuilding_thenReturnsBadRequest() throws Exception {
        mvc.perform(post("/users/1/building/modmat?bud=mud"))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void givenNoCurrentExistedUserId_whenGetBuilding_thenForbidden() throws  Exception{
        when(buildingService.existsByUser_Id(1)).thenReturn(true);
        when(userService.isCurrentUser(1)).thenReturn(false);

        mvc.perform(get("/users/1/building"))
                .andExpect(status().isForbidden());
    }

    @Test
    public void givenNoExistedUserId_whenModMatBuilding_thenNotFound() throws  Exception{
        when(buildingService.existsByUser_Id(1)).thenReturn(false);

        mvc.perform(post("/users/1/building/modmat?bud=mud&val=6"))
                .andExpect(status().isNotFound());
    }

    @Test
    public void givenNoCurrentExistedUserId_whenModRecBuilding_thenForbidden() throws  Exception{
        when(buildingService.existsByUser_Id(1)).thenReturn(true);
        when(userService.isCurrentUser(1)).thenReturn(false);

        mvc.perform(post("/users/1/building/modmat?bud=mud&val=6"))
                .andExpect(status().isForbidden());
    }
}
