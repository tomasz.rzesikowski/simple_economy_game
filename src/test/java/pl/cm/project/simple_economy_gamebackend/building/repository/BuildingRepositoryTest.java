package pl.cm.project.simple_economy_gamebackend.building.repository;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import pl.cm.project.simple_economy_gamebackend.building.model.Building;
import pl.cm.project.simple_economy_gamebackend.building.model.BuildingBuilder;
import pl.cm.project.simple_economy_gamebackend.building.properties.BuildingProperties;
import pl.cm.project.simple_economy_gamebackend.user.model.User;
import pl.cm.project.simple_economy_gamebackend.user.model.UserBuilder;
import pl.cm.project.simple_economy_gamebackend.user.repository.UserRepository;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(SpringExtension.class)
@DataJpaTest
public class BuildingRepositoryTest {

    @Autowired
    private BuildingRepository buildingRepository;

    @Autowired
    private UserRepository userRepository;

    @MockBean
    private BuildingProperties buildingProperties;

    @Test
    public void givenUser_whenExistByUserId_thenTrue() {

        Building testBuilding = BuildingBuilder
                .aBuilding()
                .withMudGatherersCottage(1000L)
                .withHuntersHut(0L)
                .withStoneQuarry(0L)
                .withOrcsPit(false)
                .withGoblinsCavern(false)
                .withTrollsCave(false)
                .build();

        User testUser = UserBuilder
                .anUser()
                .withUsername("tester")
                .withPassword("tester")
                .withRoles("USER")
                .withBuilding(testBuilding)
                .build();

        testUser = userRepository.save(testUser);
        buildingRepository.save(testBuilding);
        boolean isExist = buildingRepository.existsBuildingByUser_Id(testUser.getId());
        assertTrue(isExist);
    }

    @Test
    public void givenUser_whenExistByNotExistingUserId_thenNullBuilding() {
        User testUser = UserBuilder
                .anUser()
                .withUsername("tester")
                .withPassword("tester")
                .withRoles("USER")
                .build();

        testUser = userRepository.save(testUser);

        Building testBuilding = BuildingBuilder
                .aBuilding()
                .withMudGatherersCottage(0L)
                .withHuntersHut(0L)
                .withStoneQuarry(0L)
                .withOrcsPit(false)
                .withGoblinsCavern(false)
                .withTrollsCave(false)
                .withUser(testUser)
                .build();

        buildingRepository.save(testBuilding);
        boolean isExist = buildingRepository.existsBuildingByUser_Id(testUser.getId() + 1);
        assertFalse(isExist);
    }

    @Test
    public void givenUser_whenFindByNotExistingUserId_thenNullBuilding() {
        Building findBuilding = buildingRepository.findByUser_Id(1);

        assertNull(findBuilding);
    }
}
