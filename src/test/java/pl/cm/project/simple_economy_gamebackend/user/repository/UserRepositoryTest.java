package pl.cm.project.simple_economy_gamebackend.user.repository;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import pl.cm.project.simple_economy_gamebackend.user.model.User;
import pl.cm.project.simple_economy_gamebackend.user.model.UserBuilder;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;


@ExtendWith(SpringExtension.class)
@DataJpaTest
public class UserRepositoryTest {
    @Autowired
    private UserRepository userRepository;

    @Test
    public void givenUserRepository_whenSaveAndRetrieveUser_thenOK() {
        User testUser = UserBuilder
                .anUser()
                .withUsername("tester")
                .withPassword("tester")
                .withRoles("ADMIN")
                .build();
        User testUserWithId = userRepository.save(testUser);
        Optional<User> foundUser = userRepository.findById(testUserWithId.getId());

        assertNotNull(foundUser);
        if (foundUser.isPresent()) {
            assertEquals(testUser.getUsername(), foundUser.get().getUsername());
            assertEquals(testUser.getPassword(), foundUser.get().getPassword());
        }
    }

    @Test
    public void givenUserWithoutUsername_whenSave_thenError() {
        User testUser = UserBuilder
                .anUser()
                .withPassword("tester")
                .withRoles("ADMIN")
                .build();
        Assertions.assertThrows(javax.validation.ConstraintViolationException.class,
                () -> userRepository.save(testUser));

    }

    @Test
    public void givenUserWithoutPassword_whenSave_thenError() {
        User testUser = UserBuilder
                .anUser()
                .withUsername("tester")
                .withRoles("ADMIN")
                .build();
        Assertions.assertThrows(javax.validation.ConstraintViolationException.class,
                () -> userRepository.save(testUser));
    }

    @Test
    public void givenUserWithoutRoles_whenSave_thenError() {
        User testUser = UserBuilder
                .anUser()
                .withUsername("tester")
                .withPassword("tester")
                .build();
        Assertions.assertThrows(javax.validation.ConstraintViolationException.class,
                () -> userRepository.save(testUser));
    }
}
