package pl.cm.project.simple_economy_gamebackend.user.service;

import org.junit.Assert;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import pl.cm.project.simple_economy_gamebackend.user.model.SecurityUser;
import pl.cm.project.simple_economy_gamebackend.user.model.User;
import pl.cm.project.simple_economy_gamebackend.user.model.UserBuilder;
import pl.cm.project.simple_economy_gamebackend.user.repository.UserRepository;

import java.lang.reflect.Field;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;
import static org.springframework.security.core.context.SecurityContextHolder.getContext;

@ExtendWith(SpringExtension.class)
class UserServiceTest {

    @TestConfiguration
    static class UserServiceTestContextConfiguration {

        @Bean
        public UserService userService() {
            return new UserService();
        }
    }

    @Autowired
    private UserService userService;

    @MockBean
    private UserRepository userRepository;

    @Test
    public void givenUser_whenSaveAndRetrieveUserWithEncoding_thenOK() throws NoSuchFieldException, IllegalAccessException {
        User testUser = UserBuilder
                .anUser()
                .withUsername("tester")
                .withPassword("tester")
                .withRoles("ADMIN")
                .build();

        User responseUser = UserBuilder
                .anUser()
                .withUsername("tester")
                .withPassword("tester")
                .withRoles("ADMIN")
                .build();

        String encodePassword = new BCryptPasswordEncoder().encode(responseUser.getPassword());
        responseUser.setPassword(encodePassword);

        Field privateIdField = User.class.getDeclaredField("id");
        privateIdField.setAccessible(true);
        privateIdField.set(responseUser, 1);

        when(userRepository.save(testUser)).thenReturn(responseUser);
        User testUserWithId = userService.addUser(testUser);

        when(userRepository.findById(testUserWithId.getId())).thenReturn(Optional.of(responseUser));
        Optional<User> foundUser = userService.findUserById(testUserWithId.getId());

        assertNotNull(foundUser);
        if (foundUser.isPresent()) {
            assertEquals(testUser.getUsername(), foundUser.get().getUsername());
            assertTrue(new BCryptPasswordEncoder().matches("tester", foundUser.get().getPassword()));
            assertEquals(testUser.getRoles(), foundUser.get().getRoles());
        }
    }

    @Test
    public void givenUserWithOneRole_whenSaveAndRetrieveSecurityUser_thenOK() {
        User testUser = UserBuilder
                .anUser()
                .withUsername("tester")
                .withPassword("tester")
                .withRoles("ADMIN")
                .build();

        String encodePassword = new BCryptPasswordEncoder().encode(testUser.getPassword());
        testUser.setPassword(encodePassword);

        when(userRepository.findByUsername(testUser.getUsername())).thenReturn(testUser);
        when(userRepository.existsByUsername(testUser.getUsername())).thenReturn(true);
        SecurityUser securityUser = userService.getSecurityUser(testUser.getUsername());

        assertEquals(securityUser.getUsername(), testUser.getUsername());
        assertEquals(securityUser.getPassword(), testUser.getPassword());
        assertEquals(securityUser.getAuthorities().size(), 1);
    }

    @Test
    public void givenUserWithMoreThanOneRole_whenSaveAndRetrieveSecurityUser_thenOK() {
        User testUser = UserBuilder
                .anUser()
                .withUsername("tester")
                .withPassword("tester")
                .withRoles("ADMIN,USER")
                .build();

        String encodePassword = new BCryptPasswordEncoder().encode(testUser.getPassword());
        testUser.setPassword(encodePassword);

        when(userRepository.findByUsername(testUser.getUsername())).thenReturn(testUser);
        when(userRepository.existsByUsername(testUser.getUsername())).thenReturn(true);
        SecurityUser securityUser = userService.getSecurityUser(testUser.getUsername());

        assertEquals(securityUser.getUsername(), testUser.getUsername());
        assertEquals(securityUser.getPassword(), testUser.getPassword());
        assertEquals(securityUser.getAuthorities().size(), 2);
    }

    @Test
    public void givenUserWithNotExistsUsername_whenSaveAndRetrieveSecurityUser_thenUsernameNotFoundException() {
        when(userRepository.existsByUsername("tester")).thenReturn(false);

        Assertions.assertThrows(UsernameNotFoundException.class,
                () -> userService.getSecurityUser("tester"));
    }

    @Test
    @WithMockUser(username = "tester")
    public void givenCurrentAndExistedUserId_whenIsCurrentUser_thenTrue() {
        User testUser = UserBuilder
                .anUser()
                .withUsername("tester")
                .withPassword("tester")
                .withRoles("ADMIN,USER")
                .build();

        when(userRepository.findById(1)).thenReturn(Optional.of(testUser));

        boolean isCurrent = userService.isCurrentUser(1);
        assertTrue(isCurrent);
    }

    @Test
    @WithMockUser(username = "notTester")
    public void givenNotCurrentAndExistedUserId_whenIsCurrentUser_thenFalse() {
        User testUser = UserBuilder
                .anUser()
                .withUsername("tester")
                .withPassword("tester")
                .withRoles("ADMIN,USER")
                .build();

        when(userRepository.findById(1)).thenReturn(Optional.of(testUser));

        boolean isCurrent = userService.isCurrentUser(1);
        assertFalse(isCurrent);
    }
}
