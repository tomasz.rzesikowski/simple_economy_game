package pl.cm.project.simple_economy_gamebackend.user.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import pl.cm.project.simple_economy_gamebackend.building.model.Building;
import pl.cm.project.simple_economy_gamebackend.building.model.BuildingBuilder;
import pl.cm.project.simple_economy_gamebackend.resource.model.Resource;
import pl.cm.project.simple_economy_gamebackend.resource.model.ResourceBuilder;
import pl.cm.project.simple_economy_gamebackend.unit.model.Unit;
import pl.cm.project.simple_economy_gamebackend.unit.model.UnitBuilder;
import pl.cm.project.simple_economy_gamebackend.user.model.User;
import pl.cm.project.simple_economy_gamebackend.user.model.UserBuilder;
import pl.cm.project.simple_economy_gamebackend.user.service.UserService;

import javax.validation.ConstraintViolationException;
import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.List;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@WebMvcTest(UserController.class)
class UserControllerMVCTest {
    @MockBean
    private UserService userService;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private MockMvc mvc;

    @WithMockUser(username = "tester")
    @Test
    void givenAuthenticationAndUser_whenAddNewUser_thenReturnsIsCreated() throws Exception {
        Building testBuilding = BuildingBuilder.aBuilding().defaultBuild();
        Unit testUnit = UnitBuilder.anUnit().defaultBuild();
        Resource testResource = ResourceBuilder.aResource().defaultBuild();

        User testUser = UserBuilder
                .anUser()
                .withUsername("tester")
                .withPassword("tester")
                .withRoles("ADMIN")
                .withBuilding(testBuilding)
                .withUnit(testUnit)
                .withResource(testResource)
                .build();

        User testUserWithId = UserBuilder
                .anUser()
                .withUsername("tester")
                .withPassword("tester")
                .withRoles("ADMIN")
                .withBuilding(testBuilding)
                .withUnit(testUnit)
                .withResource(testResource)
                .build();

        Field privateIdField = User.class.getDeclaredField("id");
        privateIdField.setAccessible(true);
        privateIdField.set(testUserWithId, 1);

        when(userService.addUser(testUser)).thenReturn(testUserWithId);

        mvc.perform(post("/add")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(testUser)))
                .andExpect(status().isCreated());
    }

    @Test
    void givenNotAuthenticationAndUser_whenAddNewUser_thenReturns401() throws Exception {
        User testUser = UserBuilder
                .anUser()
                .withUsername("tester")
                .withPassword("tester")
                .withRoles("ADMIN")
                .build();

        mvc.perform(post("/users/")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(testUser)))
                .andExpect(status().isUnauthorized());
    }

    @WithMockUser(username = "tester")
    @Test
    void givenAuthenticationAndUser_whenAddNewUserWithoutName_thenReturnsBAD_REQUEST() throws Exception {
        Building testBuilding = BuildingBuilder.aBuilding().defaultBuild();
        Unit testUnit = UnitBuilder.anUnit().defaultBuild();
        Resource testResource = ResourceBuilder.aResource().defaultBuild();

        User testUser = UserBuilder
                .anUser()
                .withUsername("tester")
                .withPassword("tester")
                .withRoles("ADMIN")
                .withBuilding(testBuilding)
                .withUnit(testUnit)
                .withResource(testResource)
                .build();

        when(userService.addUser(testUser)).thenThrow(ConstraintViolationException.class);

        mvc.perform(post("/add")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(testUser)))
                .andExpect(status().isBadRequest());
    }

    @Test
    void givenNotAuthenticationAndUser_whenAddNewUserWithoutName_thenReturns401() throws Exception {
        User testUser = UserBuilder
                .anUser()
                .withPassword("tester")
                .withRoles("ADMIN")
                .build();

        when(userService.addUser(testUser)).thenThrow(ConstraintViolationException.class);

        mvc.perform(post("/users/")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(testUser)))
                .andExpect(status().isUnauthorized());
    }

    @WithMockUser(username = "tester")
    @Test
    void givenAuthenticationAndUser_whenAddNewUserWithoutPassword_thenReturnsBAD_REQUEST() throws Exception {
        Building testBuilding = BuildingBuilder.aBuilding().defaultBuild();
        Unit testUnit = UnitBuilder.anUnit().defaultBuild();
        Resource testResource = ResourceBuilder.aResource().defaultBuild();

        User testUser = UserBuilder
                .anUser()
                .withUsername("tester")
                .withPassword("tester")
                .withRoles("ADMIN")
                .withBuilding(testBuilding)
                .withUnit(testUnit)
                .withResource(testResource)
                .build();

        when(userService.addUser(testUser)).thenThrow(ConstraintViolationException.class);

        mvc.perform(post("/add")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(testUser)))
                .andExpect(status().isBadRequest());
    }

    @WithMockUser(username = "tester", authorities = "ADMIN")
    @Test
    public void givenAuthenticationAndExistingUserId_whenDelete_thenReturnOK() throws Exception {
        when(userService.userWithIdExists(1)).thenReturn(true);

        mvc.perform(delete("/admin/{id}", 1))
        .andExpect(status().isOk());
    }

    @Test
    public void givenNotAuthenticationAndExistingUserId_whenDelete_thenReturn401() throws Exception {
        when(userService.userWithIdExists(1)).thenReturn(true);

        mvc.perform(delete("/admin/{id}", 1))
                .andExpect(status().isUnauthorized());
    }

    @WithMockUser(username = "tester", authorities = "ADMIN")
    @Test
    public void givenAuthenticationAndNotExistingUserId_whenDelete_thenReturnNOT_FOUND() throws Exception {
        when(userService.userWithIdExists(2)).thenReturn(false);

        mvc.perform(delete("/admin/{id}", 2))
                .andExpect(status().isNotFound());
    }

    @Test
    public void givenNotAuthenticationAndNotExistingUserId_whenDelete_thenReturn401() throws Exception {
        when(userService.userWithIdExists(2)).thenReturn(false);

        mvc.perform(delete("/users/{id}", 2))
                .andExpect(status().isUnauthorized());
    }


    @Test
    @WithMockUser(username = "tester", authorities = {"ADMIN"})
    public void givenAdminAuthentication_whenGetAllUsers_thenReturnUserListAndOK() throws Exception {
        User testUserWithId = UserBuilder
                .anUser()
                .withUsername("tester")
                .withPassword("tester")
                .withRoles("ADMIN")
                .build();

        Field privateIdField = User.class.getDeclaredField("id");
        privateIdField.setAccessible(true);
        privateIdField.set(testUserWithId, 1);

        List<User> users = Arrays.asList(testUserWithId);
        when(userService.findAllUsers()).thenReturn(users);

        mvc.perform(get("/admin/all"))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].id").value(1))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].username").value("tester"))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].password").value("tester"))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].roles").value("ADMIN"))
                .andExpect(MockMvcResultMatchers.jsonPath("$[1]").doesNotExist())
                .andExpect(status().isOk());
    }

    @Test
    @WithMockUser(username = "tester", authorities = {"USER"})
    public void givenUserAuthentication_whenGetAllUsers_thenReturn403() throws Exception {
        mvc.perform(get("/admin/all"))
                .andExpect(status().isForbidden());
    }

    @Test
    @WithMockUser(username = "tester", authorities = {"ADMIN"})
    public void givenAdminAuthentication_whenGetAllUsersInXML_thenReturnUNSUPPORTED_MEDIA_TYPE() throws Exception {
        User testUserWithId = UserBuilder
                .anUser()
                .withUsername("tester")
                .withPassword("tester")
                .withRoles("ADMIN")
                .build();

        Field privateIdField = User.class.getDeclaredField("id");
        privateIdField.setAccessible(true);
        privateIdField.set(testUserWithId, 1);

        List<User> users = Arrays.asList(testUserWithId);
        when(userService.findAllUsers()).thenReturn(users);

        mvc.perform(get("/admin/all").accept(MediaType.APPLICATION_XML_VALUE))
                .andExpect(status().isNotAcceptable());
    }
}
