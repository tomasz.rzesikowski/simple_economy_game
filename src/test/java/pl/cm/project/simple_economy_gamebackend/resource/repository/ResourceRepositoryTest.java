package pl.cm.project.simple_economy_gamebackend.resource.repository;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import pl.cm.project.simple_economy_gamebackend.resource.model.Resource;
import pl.cm.project.simple_economy_gamebackend.resource.model.ResourceBuilder;
import pl.cm.project.simple_economy_gamebackend.user.model.User;
import pl.cm.project.simple_economy_gamebackend.user.model.UserBuilder;
import pl.cm.project.simple_economy_gamebackend.user.repository.UserRepository;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(SpringExtension.class)
@DataJpaTest
class ResourceRepositoryTest {

    @Autowired
    private ResourceRepository resourceRepository;
    @Autowired
    private UserRepository userRepository;

    @Test
    public void whenSaveNewResourceAndFindById_thenReturnResource() {

        User testUser = UserBuilder
                .anUser()
                .withUsername("tester")
                .withPassword("tester")
                .withRoles("ADMIN")
                .build();
        testUser = userRepository.save(testUser);
        Resource testResource = ResourceBuilder.aResource()
                .withMudQuantity(200L)
                .withStoneQuantity(100L)
                .withMeatQuantity(0L)
                .withUser(testUser)
                .build();

        Resource resourceWithId = resourceRepository.save(testResource);
        Resource foundResource = resourceRepository.findById(resourceWithId.getId()).orElse(new Resource());

        assertAll("Resources not equal",
                () -> assertEquals(testResource.getMudQuantity(), foundResource.getMudQuantity()),
                () -> assertEquals(testResource.getStoneQuantity(), foundResource.getStoneQuantity()),
                () -> assertEquals(testResource.getMeatQuantity(), foundResource.getMeatQuantity()));
    }

    @Test
    public void whenFindByNotExistingUserId_thenReturnNull() {
        Resource foundResource = resourceRepository.findByUserId(-1);

        assertNull(foundResource);
    }

    @Test
    public void whenExistByExistingUserId_thenTrue() {
        Resource testResource = ResourceBuilder.aResource()
                .withMudQuantity(200L)
                .withStoneQuantity(100L)
                .withMeatQuantity(0L)
                .build();


        User testUser = UserBuilder
                .anUser()
                .withUsername("tester")
                .withPassword("tester")
                .withRoles("USER")
                .withResource(testResource)
                .build();



        testUser = userRepository.save(testUser);


        resourceRepository.save(testResource);
        boolean isExist = resourceRepository.existsByUser_Id(testUser.getId());

        assertTrue(isExist);
    }

    @Test
    public void whenExistByNotExistingUserId_thenReturnNull() {

        boolean isExist = resourceRepository.existsByUser_Id(-1);

        assertFalse(isExist);
    }

}
