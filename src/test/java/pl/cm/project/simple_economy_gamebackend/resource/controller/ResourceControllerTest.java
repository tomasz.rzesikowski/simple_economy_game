package pl.cm.project.simple_economy_gamebackend.resource.controller;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import pl.cm.project.simple_economy_gamebackend.resource.model.Resource;
import pl.cm.project.simple_economy_gamebackend.resource.model.ResourceBuilder;
import pl.cm.project.simple_economy_gamebackend.resource.service.ResourceService;
import pl.cm.project.simple_economy_gamebackend.user.model.User;
import pl.cm.project.simple_economy_gamebackend.user.model.UserBuilder;
import pl.cm.project.simple_economy_gamebackend.user.service.UserService;

import java.lang.reflect.Field;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@ExtendWith(SpringExtension.class)
@WebMvcTest(ResourceController.class)
class ResourceControllerTest {

    @Autowired
    private MockMvc mvc;
    @MockBean
    private ResourceService resourceService;
    @MockBean
    private UserService userService;

    @WithMockUser(username = "tester")
    @Test
    public void givenUserAndResource_whenGetResource_thenReturnResourceDto() throws Exception {
        User testUser = UserBuilder
                .anUser()
                .withUsername("tester")
                .withPassword("tester")
                .withRoles("USER")
                .build();
        Field userIdField = User.class.getDeclaredField("id");
        userIdField.setAccessible(true);
        userIdField.set(testUser, 1);

        Resource testResource = ResourceBuilder.aResource()
                .withMudQuantity(200L)
                .withStoneQuantity(100L)
                .withMeatQuantity(0L)
                .withUser(testUser)
                .build();

        when(resourceService.existsByUserId(1)).thenReturn(true);
        when(resourceService.findResourceByUserId(1)).thenReturn(testResource);
        when(userService.isCurrentUser(1)).thenReturn(true);

        mvc.perform(get("/users/1/resources")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("mudQuantity").value(testResource.getMudQuantity()))
                .andExpect(jsonPath("stoneQuantity").value(testResource.getStoneQuantity()))
                .andExpect(jsonPath("meatQuantity").value(testResource.getMeatQuantity()));
    }

    @WithMockUser(username = "tester")
    @Test
    public void whenUserNotFound_thenReturnNotFoundStatus() throws Exception {
        when(resourceService.existsByUserId(1)).thenReturn(false);

        mvc.perform(get("/users/1/resources"))
                .andExpect(status().isNotFound());
    }

    @WithMockUser(username = "tester")
    @Test
    public void whenUserIdDontMatchLoggedUser_thenReturnNotFoundStatus() throws Exception {
        when(resourceService.existsByUserId(1)).thenReturn(true);
        when(userService.isCurrentUser(1)).thenReturn(false);

        mvc.perform(get("/users/1/resources"))
                .andExpect(status().isForbidden());
    }
}
