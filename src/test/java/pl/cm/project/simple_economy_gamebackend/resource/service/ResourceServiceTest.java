package pl.cm.project.simple_economy_gamebackend.resource.service;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import pl.cm.project.simple_economy_gamebackend.resource.model.Resource;
import pl.cm.project.simple_economy_gamebackend.resource.model.ResourceBuilder;
import pl.cm.project.simple_economy_gamebackend.resource.repository.ResourceRepository;
import pl.cm.project.simple_economy_gamebackend.user.model.User;
import pl.cm.project.simple_economy_gamebackend.user.model.UserBuilder;

import java.lang.reflect.Field;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)
class ResourceServiceTest {

    @TestConfiguration
    static class ServiceTestContextConfiguration {

        @Bean
        public ResourceService resourceService() {
            return new ResourceService();
        }
    }

    @Autowired
    private ResourceService resourceService;
    @MockBean
    private ResourceRepository resourceRepository;

    @Test
    public void whenGivenUserId_thenResourceShouldBeFoundByUserIdAndHaveProperValues()
            throws NoSuchFieldException, IllegalAccessException {
        User testUser = UserBuilder
                .anUser()
                .withUsername("tester")
                .withPassword("tester")
                .build();
        Field userIdField = User.class.getDeclaredField("id");
        userIdField.setAccessible(true);
        userIdField.set(testUser, 1);

        Resource testResource = ResourceBuilder.aResource()
                .withMudQuantity(200L)
                .withStoneQuantity(100L)
                .withMeatQuantity(0L)
                .withUser(testUser)
                .build();
        Field resourceIdField = Resource.class.getDeclaredField("id");
        resourceIdField.setAccessible(true);
        resourceIdField.set(testResource, 1);
        when(resourceRepository.findByUserId(testUser.getId()))
                .thenReturn(testResource);

        Resource resource = resourceService.findResourceByUserId(1);

        assertAll("Resources not equal",
                () -> assertEquals(resource.getId(), 1),
                () -> assertEquals(resource.getMudQuantity(), 200L),
                () -> assertEquals(resource.getStoneQuantity(), 100L),
                () -> assertEquals(resource.getMeatQuantity(), 0));
    }

    @Test
    public void whenGivenExistingUserId_thenResourceShouldExist() {

        when(resourceRepository.existsByUser_Id(1))
                .thenReturn(true);

        assertTrue(resourceService.existsByUserId(1));
    }

    @Test
    public void givenUserId_whenAddingMudAndStoneAndMeat_thenQValuesShouldIncreaseProperly()
            throws NoSuchFieldException, IllegalAccessException {
        User testUser = UserBuilder
                .anUser()
                .withUsername("tester")
                .withPassword("tester")
                .build();
        Field userIdField = User.class.getDeclaredField("id");
        userIdField.setAccessible(true);
        userIdField.set(testUser, 1);

        Resource testResource = ResourceBuilder.aResource()
                .withMudQuantity(2000L)
                .withStoneQuantity(2000L)
                .withMeatQuantity(2000L)
                .withUser(testUser)
                .build();
        Field resourceIdField = Resource.class.getDeclaredField("id");
        resourceIdField.setAccessible(true);
        resourceIdField.set(testResource, 1);
        when(resourceRepository.findByUserId(testUser.getId()))
                .thenReturn(testResource);

        resourceService.changeMudQuantity(testUser.getId(), 1000L);
        resourceService.changeStoneQuantity(testUser.getId(), 1000L);
        resourceService.changeMeatQuantity(testUser.getId(), 1000L);

        assertAll("Resources not equal",
                () -> assertEquals(testResource.getMudQuantity(), 3000L),
                () -> assertEquals(testResource.getStoneQuantity(), 3000L),
                () -> assertEquals(testResource.getMeatQuantity(), 3000L));
    }

    @Test
    public void givenUserId_whenSubtractingMudAndStoneAndMeat_thenValuesShouldDecreaseProperly()
            throws NoSuchFieldException, IllegalAccessException {
        User testUser = UserBuilder
                .anUser()
                .withUsername("tester")
                .withPassword("tester")
                .build();
        Field userIdField = User.class.getDeclaredField("id");
        userIdField.setAccessible(true);
        userIdField.set(testUser, 1);

        Resource testResource = ResourceBuilder.aResource()
                .withMudQuantity(2000L)
                .withStoneQuantity(2000L)
                .withMeatQuantity(2000L)
                .withUser(testUser)
                .build();
        Field resourceIdField = Resource.class.getDeclaredField("id");
        resourceIdField.setAccessible(true);
        resourceIdField.set(testResource, 1);
        when(resourceRepository.findByUserId(testUser.getId()))
                .thenReturn(testResource);

        resourceService.changeMudQuantity(testUser.getId(), -1000L);
        resourceService.changeStoneQuantity(testUser.getId(), -1000L);
        resourceService.changeMeatQuantity(testUser.getId(), -1000L);

        assertAll("Resources not equal",
                () -> assertEquals(testResource.getMudQuantity(), 1000L),
                () -> assertEquals(testResource.getStoneQuantity(), 1000L),
                () -> assertEquals(testResource.getMeatQuantity(), 1000L));
    }

    @Test
    public void givenUserId_whenTryingToChangeMudAndStoneAndMeatBelow0_thenValuesShouldBe0()
            throws NoSuchFieldException, IllegalAccessException {
        User testUser = UserBuilder
                .anUser()
                .withUsername("tester")
                .withPassword("tester")
                .build();
        Field userIdField = User.class.getDeclaredField("id");
        userIdField.setAccessible(true);
        userIdField.set(testUser, 1);

        Resource testResource = ResourceBuilder.aResource()
                .withMudQuantity(2000L)
                .withStoneQuantity(2000L)
                .withMeatQuantity(2000L)
                .withUser(testUser)
                .build();
        Field resourceIdField = Resource.class.getDeclaredField("id");
        resourceIdField.setAccessible(true);
        resourceIdField.set(testResource, 1);
        when(resourceRepository.findByUserId(testUser.getId()))
                .thenReturn(testResource);

        resourceService.changeMudQuantity(testUser.getId(), -3000L);
        resourceService.changeStoneQuantity(testUser.getId(), -3000L);
        resourceService.changeMeatQuantity(testUser.getId(), -3000L);

        assertAll("Resources not equal",
                () -> assertEquals(testResource.getMudQuantity(), 0L),
                () -> assertEquals(testResource.getStoneQuantity(), 0L),
                () -> assertEquals(testResource.getMeatQuantity(), 0L));
    }
}
