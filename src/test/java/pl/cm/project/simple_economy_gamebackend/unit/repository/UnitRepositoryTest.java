package pl.cm.project.simple_economy_gamebackend.unit.repository;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import pl.cm.project.simple_economy_gamebackend.unit.model.Unit;
import pl.cm.project.simple_economy_gamebackend.unit.model.UnitBuilder;
import pl.cm.project.simple_economy_gamebackend.user.model.User;
import pl.cm.project.simple_economy_gamebackend.user.model.UserBuilder;
import pl.cm.project.simple_economy_gamebackend.user.repository.UserRepository;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(SpringExtension.class)
@DataJpaTest
class UnitRepositoryTest {
    @Autowired
    private UnitRepository unitRepository;

    @Autowired
    private UserRepository userRepository;

    @Test
    public void givenUser_whenFindByUserId_thenUnit() {
        User testUser = UserBuilder
                .anUser()
                .withUsername("tester")
                .withPassword("tester")
                .withRoles("USER")
                .build();

        testUser = userRepository.save(testUser);

        Unit testUnit = UnitBuilder
                .anUnit()
                .withGoblinArcherQuantity(0L)
                .withOrcWarriorQuantity(0L)
                .withUglyTrollQuantity(3L)
                .withUser(testUser)
                .build();

        Unit respondUnit = unitRepository.save(testUnit);

        testUser.setUnit(testUnit);
        testUser = userRepository.save(testUser);

        Unit findUnit = unitRepository.findUnitByUser_Id(testUser.getId());

        assertNotNull(findUnit);
        assertEquals(respondUnit.getId(), findUnit.getId());
        assertEquals(respondUnit.getGoblinArcherQuantity(), findUnit.getGoblinArcherQuantity());
        assertEquals(respondUnit.getOrcWarriorQuantity(), findUnit.getOrcWarriorQuantity());
        assertEquals(respondUnit.getUglyTrollQuantity(), findUnit.getUglyTrollQuantity());
        assertEquals(respondUnit.getUser(), findUnit.getUser());
    }

    @Test
    public void givenUser_whenFindByNotExistingUserId_thenNullUnit() {
        Unit findUnit = unitRepository.findUnitByUser_Id(999999999);

        assertNull(findUnit);
    }

    @Test
    public void givenUser_whenExistByUserId_thenTrue() {
        Unit testUnit = UnitBuilder
                .anUnit()
                .withGoblinArcherQuantity(0L)
                .withOrcWarriorQuantity(0L)
                .withUglyTrollQuantity(3L)
                .build();

        User testUser = UserBuilder
                .anUser()
                .withUsername("tester")
                .withPassword("tester")
                .withRoles("USER")
                .withUnit(testUnit)
                .build();



        testUser = userRepository.save(testUser);
        unitRepository.save(testUnit);
        boolean isExist = unitRepository.existsUnitByUser_Id(testUser.getId());

        assertTrue(isExist);
    }

    @Test
    public void givenUser_whenExistByNotExistingUserId_thenNullUnit() {
        User testUser = UserBuilder
                .anUser()
                .withUsername("tester")
                .withPassword("tester")
                .withRoles("USER")
                .build();

        testUser = userRepository.save(testUser);

        Unit testUnit = UnitBuilder
                .anUnit()
                .withGoblinArcherQuantity(0L)
                .withOrcWarriorQuantity(0L)
                .withUglyTrollQuantity(3L)
                .withUser(testUser)
                .build();

        unitRepository.save(testUnit);
        boolean isExist = unitRepository.existsUnitByUser_Id(testUser.getId()+1);

        assertFalse(isExist);
    }
}
