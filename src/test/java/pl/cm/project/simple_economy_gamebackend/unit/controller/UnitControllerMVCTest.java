package pl.cm.project.simple_economy_gamebackend.unit.controller;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import pl.cm.project.simple_economy_gamebackend.resource.service.ResourceService;
import pl.cm.project.simple_economy_gamebackend.unit.model.Unit;
import pl.cm.project.simple_economy_gamebackend.unit.model.UnitBuilder;
import pl.cm.project.simple_economy_gamebackend.unit.properties.UnitProperties;
import pl.cm.project.simple_economy_gamebackend.unit.service.UnitService;
import pl.cm.project.simple_economy_gamebackend.user.model.User;
import pl.cm.project.simple_economy_gamebackend.user.model.UserBuilder;
import pl.cm.project.simple_economy_gamebackend.user.service.UserService;

import java.lang.reflect.Field;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


@ExtendWith(SpringExtension.class)
@WebMvcTest(UnitController.class)
@WithMockUser(username = "tester")
class UnitControllerMVCTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private UnitService unitService;

    @MockBean
    private UserService userService;

    @MockBean
    private ResourceService resourceService;

    @MockBean
    private UnitProperties unitProperties;


    @Test
    @WithMockUser(username = "tester")
    public void givenUserAndUnit_whenGetUnit_thenReturnUnits() throws Exception {

        User testUser = UserBuilder
                .anUser()
                .withUsername("tester")
                .withPassword("tester")
                .withRoles("USER")
                .build();

        Unit testUnit = UnitBuilder
                .anUnit()
                .withGoblinArcherQuantity(10L)
                .withOrcWarriorQuantity(10L)
                .withUglyTrollQuantity(2L)
                .withUser(testUser)
                .build();

        when(unitService.existsByUserId(1)).thenReturn(true);
        when(unitService.findUnitByUserId(1)).thenReturn(testUnit);
        when(userService.isCurrentUser(1)).thenReturn(true);

        mvc.perform(get("/users/1/unit")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("goblinArcherQuantity").value(testUnit.getGoblinArcherQuantity()))
                .andExpect(jsonPath("orcWarriorQuantity").value(testUnit.getOrcWarriorQuantity()))
                .andExpect(jsonPath("uglyTrollQuantity").value(testUnit.getUglyTrollQuantity()));
    }

    @Test
    public void givenNotExistedUserId_whenGetUnit_thenNotFound() throws  Exception{
        when(unitService.existsByUserId(1)).thenReturn(false);

        mvc.perform(get("/users/1/unit"))
                .andExpect(status().isNotFound());
    }

    @Test
    public void givenNotCurrentExistedUserId_whenGetUnit_thenForbidden() throws  Exception{
        when(unitService.existsByUserId(1)).thenReturn(true);
        when(userService.isCurrentUser(1)).thenReturn(false);

        mvc.perform(get("/users/1/unit"))
                .andExpect(status().isForbidden());
    }

    @Test
    void givenRequestToModGoblinInUnit_whenModUnit_thenReturnsOKAndProperUnitDto() throws Exception {
        Long qtyToMod = 3L;
        String unitType = "gob";

        User testUser = UserBuilder
                .anUser()
                .withUsername("tester")
                .withPassword("tester")
                .withRoles("USER")
                .build();

        Unit testUnit = UnitBuilder
                .anUnit()
                .withGoblinArcherQuantity(10L)
                .withOrcWarriorQuantity(10L)
                .withUglyTrollQuantity(3L)
                .withUser(testUser)
                .build();

        testUnit.setGoblinArcherQuantity(testUnit.getGoblinArcherQuantity()+qtyToMod);

        Field privateIdField = Unit.class.getDeclaredField("id");
        privateIdField.setAccessible(true);
        privateIdField.set(testUnit, 1);

        when(unitService.existsByUserId(1)).thenReturn(true);
        when(unitService.modValOfGoblinArcher(1, qtyToMod)).thenReturn(testUnit);
        when(userService.isCurrentUser(1)).thenReturn(true);
        when(unitService.canRecruitUnit(1, unitType, qtyToMod)).thenReturn(true);

        mvc.perform(post("/users/1/unit/mod?type=gob&val=3"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(jsonPath("goblinArcherQuantity").value(testUnit.getGoblinArcherQuantity()))
                .andExpect(jsonPath("orcWarriorQuantity").value(testUnit.getOrcWarriorQuantity()))
                .andExpect(jsonPath("uglyTrollQuantity").value(testUnit.getUglyTrollQuantity()));
    }

    @Test
    void givenRequestToModOrcWarriorQuantity_whenModOrcWarriorQuantity_thenReturnsOKAndProperUnitDto() throws Exception {
        Long qtyToMod = 3L;
        String unitType = "orc";

        User testUser = UserBuilder
                .anUser()
                .withUsername("tester")
                .withPassword("tester")
                .withRoles("USER")
                .build();

        Unit testUnit = UnitBuilder
                .anUnit()
                .withGoblinArcherQuantity(10L)
                .withOrcWarriorQuantity(10L)
                .withUglyTrollQuantity(3L)
                .withUser(testUser)
                .build();

        testUnit.setOrcWarriorQuantity(testUnit.getOrcWarriorQuantity()+qtyToMod);

        Field privateIdField = Unit.class.getDeclaredField("id");
        privateIdField.setAccessible(true);
        privateIdField.set(testUnit, 1);

        when(unitService.existsByUserId(1)).thenReturn(true);
        when(unitService.modValOfOrcWarrior(1, qtyToMod)).thenReturn(testUnit);
        when(userService.isCurrentUser(1)).thenReturn(true);
        when(unitService.canRecruitUnit(1, unitType, qtyToMod)).thenReturn(true);

        mvc.perform(post("/users/1/unit/mod?type=orc&val=3"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(jsonPath("goblinArcherQuantity").value(testUnit.getGoblinArcherQuantity()))
                .andExpect(jsonPath("orcWarriorQuantity").value(testUnit.getOrcWarriorQuantity()))
                .andExpect(jsonPath("uglyTrollQuantity").value(testUnit.getUglyTrollQuantity()));
    }

    @Test
    void givenRequestToModTrollInUnit_whenModUnit_thenReturnsOKAndProperUnitDto() throws Exception {
        Long qtyToMod = 3L;
        String unitType = "troll";

        User testUser = UserBuilder
                .anUser()
                .withUsername("tester")
                .withPassword("tester")
                .withRoles("USER")
                .build();

        Unit testUnit = UnitBuilder
                .anUnit()
                .withGoblinArcherQuantity(10L)
                .withOrcWarriorQuantity(5L)
                .withUglyTrollQuantity(3L)
                .withUser(testUser)
                .build();

        testUnit.setUglyTrollQuantity(testUnit.getUglyTrollQuantity()+qtyToMod);

        Field privateIdField = Unit.class.getDeclaredField("id");
        privateIdField.setAccessible(true);
        privateIdField.set(testUnit, 1);

        when(unitService.existsByUserId(1)).thenReturn(true);
        when(unitService.modValOfUglyTroll(1, qtyToMod)).thenReturn(testUnit);
        when(userService.isCurrentUser(1)).thenReturn(true);
        when(unitService.canRecruitUnit(1, unitType, qtyToMod)).thenReturn(true);

        mvc.perform(post("/users/1/unit/mod?type=troll&val=3"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(jsonPath("goblinArcherQuantity").value(testUnit.getGoblinArcherQuantity()))
                .andExpect(jsonPath("orcWarriorQuantity").value(testUnit.getOrcWarriorQuantity()))
                .andExpect(jsonPath("uglyTrollQuantity").value(testUnit.getUglyTrollQuantity()));
    }

    @Test
    void givenWrongRequestToModUnit_whenModUnit_thenReturnsBadRequest() throws Exception {
        mvc.perform(post("/users/1/unit/mod?type=gob"))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void givenNotExistedUserId_whenModUnit_thenBadRequest() throws  Exception{
        when(unitService.existsByUserId(1)).thenReturn(false);

        mvc.perform(post("/users/1/unit/mod?tpe=gob&val=3"))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void givenNotCurrentExistedUserId_whenModUnit_thenForbidden() throws  Exception{
        when(unitService.existsByUserId(1)).thenReturn(true);
        when(userService.isCurrentUser(1)).thenReturn(false);

        mvc.perform(post("/users/1/unit/mod?type=gob&val=3"))
                .andExpect(status().isForbidden());
    }

}
