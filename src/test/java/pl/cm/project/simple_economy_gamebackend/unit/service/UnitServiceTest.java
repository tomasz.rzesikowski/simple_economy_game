package pl.cm.project.simple_economy_gamebackend.unit.service;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import pl.cm.project.simple_economy_gamebackend.resource.service.ResourceService;
import pl.cm.project.simple_economy_gamebackend.unit.model.Unit;
import pl.cm.project.simple_economy_gamebackend.unit.model.UnitBuilder;
import pl.cm.project.simple_economy_gamebackend.unit.properties.UnitProperties;
import pl.cm.project.simple_economy_gamebackend.unit.repository.UnitRepository;
import pl.cm.project.simple_economy_gamebackend.user.model.User;
import pl.cm.project.simple_economy_gamebackend.user.model.UserBuilder;

import java.lang.reflect.Field;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)
class UnitServiceTest {
    @TestConfiguration
    static class ServiceTestContextConfiguration {

        @Bean
        public UnitService unitService() {
            return new UnitService();
        }
    }

    @Autowired
    private UnitService unitService;

    @MockBean
    private UnitRepository unitRepository;

    @MockBean
    private ResourceService resourceService;

    @MockBean
    private UnitProperties unitProperties;


    @Test
    void givenExistedUserId_whenFindByUserID_thenUnitShouldBeFoundByUserIdAndHaveProperValues() throws NoSuchFieldException, IllegalAccessException {
        User testUser = UserBuilder
                .anUser()
                .withUsername("tester")
                .withPassword("tester")
                .withRoles("USER")
                .build();
        Field userIdField = User.class.getDeclaredField("id");
        userIdField.setAccessible(true);
        userIdField.set(testUser, 1);

        Unit testUnit = UnitBuilder
                .anUnit()
                .withGoblinArcherQuantity(10L)
                .withOrcWarriorQuantity(10L)
                .withUglyTrollQuantity(3L)
                .withUser(testUser)
                .build();

        when(unitRepository.findUnitByUser_Id(testUser.getId())).thenReturn(testUnit);

        Unit responseUnit = unitService.findUnitByUserId(testUser.getId());

        assertNotNull(responseUnit);
        assertEquals(responseUnit.getGoblinArcherQuantity(), testUnit.getGoblinArcherQuantity());
        assertEquals(responseUnit.getOrcWarriorQuantity(), testUnit.getOrcWarriorQuantity());
        assertEquals(responseUnit.getUglyTrollQuantity(), testUnit.getUglyTrollQuantity());
        assertEquals(responseUnit.getUser(), testUnit.getUser());
    }

    @Test
    void givenNotExistedUserId_whenFindByUserID_thenReturnNUll() {
        when(unitRepository.findUnitByUser_Id(2)).thenReturn(null);
        Unit responseUnit = unitService.findUnitByUserId(2);

        assertNull(responseUnit);
    }

    @Test
    void givenExistedUserId_whenExistByUserID_thenTrue() {
        when(unitRepository.existsUnitByUser_Id(1)).thenReturn(true);

        boolean isExisted = unitService.existsByUserId(1);

        assertTrue(isExisted);
    }

    @Test
    void givenNotExistedUserId_whenExistByUserID_thenFalse() {
        when(unitRepository.existsUnitByUser_Id(1)).thenReturn(false);

        boolean isExisted = unitService.existsByUserId(1);

        assertFalse(isExisted);
    }

    @Test
    void givenExistedUserIdAndGoblinQtyAndSmallerQtyToMod_whenModValOfGoblinArcher_thenProperChange() throws NoSuchFieldException, IllegalAccessException {
        Long qtyToMod = -3L;

        User testUser = UserBuilder
                .anUser()
                .withUsername("tester")
                .withPassword("tester")
                .withRoles("USER")
                .build();
        Field userIdField = User.class.getDeclaredField("id");
        userIdField.setAccessible(true);
        userIdField.set(testUser, 1);

        Unit testUnit = UnitBuilder
                .anUnit()
                .withGoblinArcherQuantity(10L)
                .withOrcWarriorQuantity(10L)
                .withUglyTrollQuantity(3L)
                .withUser(testUser)
                .build();

        when(unitRepository.findUnitByUser_Id(testUser.getId())).thenReturn(testUnit);

        testUnit.setGoblinArcherQuantity(testUnit.getGoblinArcherQuantity()+qtyToMod);
        when(unitRepository.save(testUnit)).thenReturn(testUnit);

        Unit responseUnit = unitService.modValOfGoblinArcher(testUser.getId(), qtyToMod);

        assertNotNull(responseUnit);
        assertEquals(responseUnit.getId(), testUnit.getId());
        assertEquals(responseUnit.getGoblinArcherQuantity(), testUnit.getGoblinArcherQuantity());
        assertEquals(responseUnit.getOrcWarriorQuantity(), testUnit.getOrcWarriorQuantity());
        assertEquals(responseUnit.getUglyTrollQuantity(), testUnit.getUglyTrollQuantity());
        assertEquals(responseUnit.getUser(), testUnit.getUser());
    }

    @Test
    void givenExistedUserIdAndGoblinQtyAndGreaterQtyToMod_whenModValOfGoblinArcher_thenQtyChangeToZero() throws NoSuchFieldException, IllegalAccessException {
        Long qtyToMod = -11L;

        User testUser = UserBuilder
                .anUser()
                .withUsername("tester")
                .withPassword("tester")
                .withRoles("USER")
                .build();
        Field userIdField = User.class.getDeclaredField("id");
        userIdField.setAccessible(true);
        userIdField.set(testUser, 1);

        Unit testUnit = UnitBuilder
                .anUnit()
                .withGoblinArcherQuantity(10L)
                .withOrcWarriorQuantity(10L)
                .withUglyTrollQuantity(3L)
                .withUser(testUser)
                .build();

        when(unitRepository.findUnitByUser_Id(testUser.getId())).thenReturn(testUnit);

        testUnit.setGoblinArcherQuantity(testUnit.getGoblinArcherQuantity()+qtyToMod);
        when(unitRepository.save(testUnit)).thenReturn(testUnit);

        Unit responseUnit = unitService.modValOfGoblinArcher(testUser.getId(), qtyToMod);

        assertNotNull(responseUnit);
        assertEquals(responseUnit.getId(), testUnit.getId());
        assertEquals(responseUnit.getGoblinArcherQuantity(), 0);
        assertEquals(responseUnit.getOrcWarriorQuantity(), testUnit.getOrcWarriorQuantity());
        assertEquals(responseUnit.getUglyTrollQuantity(), testUnit.getUglyTrollQuantity());
        assertEquals(responseUnit.getUser(), testUnit.getUser());
    }

    @Test
    void givenNotExistedUserIdAndGoblinQtyAndGreaterQtyToMod_whenModValOfGoblinArcher_thenNull() {
        Long qtyToMod = -11L;
        Integer notExistedId = 1;

        when(unitRepository.findUnitByUser_Id(notExistedId)).thenReturn(null);

        Unit responseUnit = unitService.modValOfGoblinArcher(notExistedId, qtyToMod);

        assertNull(responseUnit);
    }

    @Test
    void givenExistedUserIdAndOrcQtyAndSmallerQtyToMod_whenModValOfOrcWarrior_thenProperChange()
            throws NoSuchFieldException, IllegalAccessException {
        Long qtyToMod = -3L;

        User testUser = UserBuilder
                .anUser()
                .withUsername("tester")
                .withPassword("tester")
                .withRoles("USER")
                .build();
        Field userIdField = User.class.getDeclaredField("id");
        userIdField.setAccessible(true);
        userIdField.set(testUser, 1);

        Unit testUnit = UnitBuilder
                .anUnit()
                .withGoblinArcherQuantity(10L)
                .withOrcWarriorQuantity(10L)
                .withUglyTrollQuantity(2L)
                .withUser(testUser)
                .build();

        when(unitRepository.findUnitByUser_Id(testUser.getId())).thenReturn(testUnit);

        testUnit.setOrcWarriorQuantity(testUnit.getOrcWarriorQuantity()+qtyToMod);
        when(unitRepository.save(testUnit)).thenReturn(testUnit);

        Unit responseUnit = unitService.modValOfOrcWarrior(testUser.getId(), qtyToMod);

        assertNotNull(responseUnit);
        assertEquals(responseUnit.getId(), testUnit.getId());
        assertEquals(responseUnit.getGoblinArcherQuantity(), testUnit.getGoblinArcherQuantity());
        assertEquals(responseUnit.getOrcWarriorQuantity(), testUnit.getOrcWarriorQuantity());
        assertEquals(responseUnit.getUglyTrollQuantity(), testUnit.getUglyTrollQuantity());
        assertEquals(responseUnit.getUser(), testUnit.getUser());
    }

    @Test
    void givenExistedUserIdAndOrcQtyAndGreaterQtyToMod_whenModValOfOrcWarrior_thenQtyChangeToZero()
            throws NoSuchFieldException, IllegalAccessException {
        Long qtyToMod = -11L;

        User testUser = UserBuilder
                .anUser()
                .withUsername("tester")
                .withPassword("tester")
                .withRoles("USER")
                .build();
        Field userIdField = User.class.getDeclaredField("id");
        userIdField.setAccessible(true);
        userIdField.set(testUser, 1);

        Unit testUnit = UnitBuilder
                .anUnit()
                .withGoblinArcherQuantity(10L)
                .withOrcWarriorQuantity(10L)
                .withUglyTrollQuantity(3L)
                .withUser(testUser)
                .build();

        when(unitRepository.findUnitByUser_Id(testUser.getId())).thenReturn(testUnit);

        testUnit.setOrcWarriorQuantity(testUnit.getOrcWarriorQuantity()+qtyToMod);
        when(unitRepository.save(testUnit)).thenReturn(testUnit);

        Unit responseUnit = unitService.modValOfOrcWarrior(testUser.getId(), qtyToMod);

        assertNotNull(responseUnit);
        assertEquals(responseUnit.getId(), testUnit.getId());
        assertEquals(responseUnit.getGoblinArcherQuantity(), testUnit.getGoblinArcherQuantity());
        assertEquals(responseUnit.getOrcWarriorQuantity(), 0);
        assertEquals(responseUnit.getUglyTrollQuantity(), testUnit.getUglyTrollQuantity());
        assertEquals(responseUnit.getUser(), testUnit.getUser());
    }

    @Test
    void givenNotExistedUserIdAndOrcQtyAndGreaterQtyToMod_whenModValOfOrcWarrior_thenNull() {
        Long qtyToMod = -11L;
        Integer notExistedId = 1;

        when(unitRepository.findUnitByUser_Id(notExistedId)).thenReturn(null);

        Unit responseUnit = unitService.modValOfOrcWarrior(notExistedId, qtyToMod);

        assertNull(responseUnit);
    }

    @Test
    void givenExistedUserIdAndTrollQtyAndSmallerQtyToMod_whenModValOfUglyTroll_thenProperChange() throws NoSuchFieldException, IllegalAccessException {
        Long qtyToMod = -3L;

        User testUser = UserBuilder
                .anUser()
                .withUsername("tester")
                .withPassword("tester")
                .withRoles("USER")
                .build();
        Field userIdField = User.class.getDeclaredField("id");
        userIdField.setAccessible(true);
        userIdField.set(testUser, 1);

        Unit testUnit = UnitBuilder
                .anUnit()
                .withGoblinArcherQuantity(10L)
                .withUglyTrollQuantity(3L)
                .withUser(testUser)
                .build();

        when(unitRepository.findUnitByUser_Id(testUser.getId())).thenReturn(testUnit);

        testUnit.setUglyTrollQuantity(testUnit.getUglyTrollQuantity()+qtyToMod);
        when(unitRepository.save(testUnit)).thenReturn(testUnit);

        Unit responseUnit = unitService.modValOfUglyTroll(testUser.getId(), qtyToMod);

        assertNotNull(responseUnit);
        assertEquals(responseUnit.getId(), testUnit.getId());
        assertEquals(responseUnit.getGoblinArcherQuantity(), testUnit.getGoblinArcherQuantity());
        assertEquals(responseUnit.getUglyTrollQuantity(), testUnit.getUglyTrollQuantity());
        assertEquals(responseUnit.getUser(), testUnit.getUser());
    }

    @Test
    void givenExistedUserIdAndTrollQtyAndGreaterQtyToMod_whenModValOfUglyTroll_thenQtyChangeToZero() throws NoSuchFieldException, IllegalAccessException {
        Long qtyToMod = -11L;

        User testUser = UserBuilder
                .anUser()
                .withUsername("tester")
                .withPassword("tester")
                .withRoles("USER")
                .build();
        Field userIdField = User.class.getDeclaredField("id");
        userIdField.setAccessible(true);
        userIdField.set(testUser, 1);

        Unit testUnit = UnitBuilder
                .anUnit()
                .withGoblinArcherQuantity(10L)
                .withUglyTrollQuantity(3L)
                .withUser(testUser)
                .build();

        when(unitRepository.findUnitByUser_Id(testUser.getId())).thenReturn(testUnit);

        testUnit.setUglyTrollQuantity(testUnit.getUglyTrollQuantity()+qtyToMod);
        when(unitRepository.save(testUnit)).thenReturn(testUnit);

        Unit responseUnit = unitService.modValOfUglyTroll(testUser.getId(), qtyToMod);

        assertNotNull(responseUnit);
        assertEquals(responseUnit.getId(), testUnit.getId());
        assertEquals(responseUnit.getGoblinArcherQuantity(), testUnit.getGoblinArcherQuantity());
        assertEquals(responseUnit.getUglyTrollQuantity(), 0);
        assertEquals(responseUnit.getUser(), testUnit.getUser());
    }

    @Test
    void givenNotExistedUserIdAndTrollQtyAndGreaterQtyToMod_whenModValOfUglyTroll_thenNull() {
        Long qtyToMod = -11L;
        Integer notExistedId = 1;

        when(unitRepository.findUnitByUser_Id(notExistedId)).thenReturn(null);

        Unit responseUnit = unitService.modValOfUglyTroll(notExistedId, qtyToMod);

        assertNull(responseUnit);
    }
}


